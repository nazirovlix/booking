<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 27.12.2018
 * Time: 1:23
 */

use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
$this->title = 'Бронировать';
?>
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <h3>Запольните данные для продолжения</h3>
        <?php $form = ActiveForm::begin() ?>
        <div class="row" style="max-width: 800px">
            <div class="col-md-6">
                <?= $form->field($user, 'email')->textInput(['type' => 'email', 'placeholder' => 'user@mail.ru']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($user, 'password')->textInput(['type' => 'password', 'placeholder' => '******']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($userInfo, 'fio')->textInput(['placeholder' => 'Иванов Иван Иванович']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($userInfo, 'possport_n')->textInput(['placeholder' => 'AA123457']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($userInfo, 'phone')->textInput(['placeholder' => '+7123456789']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($userInfo, 'bday')->widget(DatePicker::className(), [
                    'name' => 'dp_2',
                    'type' => DatePicker::TYPE_INPUT,

                    'pluginOptions' => [
                        'defaultViewDate' => '01.01.1990',
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',

                    ]
                ])?>

            </div>
            <div class="col-md-12">
                <div class="panel-group" id="accordion" role="tablist">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4>Адрес доставки</h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <p>Доставка по городу <b>1 000 руб.</b></p>
                                <div class="col-md-4">
                                    <?= $form->field($address, 'city')->textInput(['placeholder' => 'Город'])->label(false) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($address, 'address')->textInput(['placeholder' => 'Адрес'])->label(false) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($address, 'dom')->textInput(['placeholder' => 'Дом / квартира'])->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <h4>Варианты оплаты</h4>
                <div class="radio">
                    <label>
                        <input type="radio" value="3000" name="radioAddress" class="officeAddress address-cursor"><span class="circle"></span><span class="check"></span>
                        Оплатить картой и создать бронирование
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" value="4000" name="radioAddress" class="officeAddress address-cursor"><span class="circle"></span><span class="check"></span>
                        Создать бронирование (с оплатой на месте)
                    </label>
                </div>
<!--                <div class="checkbox">-->
<!--                    <label>-->
<!--                        <input type="checkbox" name="addressOffice" class="officeAddress"><span class="checkbox-material"><span-->
<!--                                    class="check"></span></span>-->
<!--                        Забрать из офиса-->
<!--                    </label>-->
<!--                </div>-->
            </div>

        </div>



        <button type="submit" class="btn btn-primary">Продолжить</button>

        <?php ActiveForm::end() ?>
    </div>
</div>