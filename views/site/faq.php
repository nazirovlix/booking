<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 29.12.2018
 * Time: 19:52
 */
$this->title = 'F.A.Q';
?>

<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="padding-top: 30px; min-height: 400px">
            <div class="row faq-content">
                <div class="col-md-8">
                    <?php if (!empty($category)): ?>
                        <ul class="nav nav-pills nav-pills-rose faq-content">

                            <?php foreach ($category as $k => $item): ?>
                                <li class="<?= $k == 0 ? 'active' : '' ?>">
                                    <a href="#cat<?= $k ?>" data-toggle="tab"
                                       aria-expanded="false"><?= $item->name ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content tab-space">
                            <?php foreach ($category as $k => $item): ?>
                                <div class="tab-pane <?= $k == 0 ? 'active' : '' ?>" id="cat<?= $k ?>">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php if (!empty($item->faqs)): ?>
                                            <?php foreach ($item->faqs as $j => $faq): ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="heading<?= $j.$k ?>">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                           href="#collapse<?= $j.$k  ?>" aria-expanded="false"
                                                           aria-controls="collapseOne" class="collapsed">
                                                            <h4 class="panel-title">
                                                                <?= $faq->question ?>
                                                                <i class="material-icons">keyboard_arrow_down</i>
                                                            </h4>
                                                        </a>
                                                    </div>
                                                    <div id="collapse<?= $j.$k  ?>" class="panel-collapse collapse"
                                                         role="tabpanel" aria-labelledby="heading<?= $j.$k  ?>"
                                                         aria-expanded="false" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <?= $faq->answer ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>
