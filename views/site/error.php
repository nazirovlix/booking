<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">

    <div class="container" style="min-height: 400px">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="profile">
                    <div class="name">
                        <h3 class="title">404 - СТРАНИЦА НЕ НАЙДЕНА</h3>
                        <h6><a href="/" class="btn btn-primary">Перейти на главную страницу</a></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
