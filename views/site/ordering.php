<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 21.12.2018
 * Time: 13:49
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = 'Бронировать';
?>

<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container ">

        <h3>Выберите или добавьте адрес для доставки</h3>
        <p>Цена доставки: <b>1 000 руб</b></p>
        <div class="table-responsive" style="margin-top: 40px; margin-bottom: 40px">
            <table class="table">
                <thead>
                <tr>
                    <th style="width: 3%"></th>
                    <th class="text-center">#</th>
                    <th>Город</th>
                    <th>Адрес</th>
                    <th>Дом</th>

                </tr>
                </thead>
                <tbody id="addresses">
                <form action="<?= Url::to(['/site/yandex']) ?>">
                <?php if (!empty($address)) {
                    foreach ($address as $k => $item): ?>
                        <tr class="address-cursor addr">
                            <td>
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="<?= $item->id ?>" name="radioAddress" class="check-address"><span class="circle"></span><span class="check"></span>
                                    </label>
                                </div>
                            </td>
                            <td class="text-center"><?= ++$k ?></td>
                            <td class="text-left"><?= $item->id ?></td>
                            <td class="text-left"><?= $item->address ?></td>
                            <td class="text-left"><?= $item->dom ?></td>
                            <td class="td-actions text-right">
                                <button type="button" rel="tooltip"
                                        class="btn btn-success change-address" data-toggle="modal"
                                        data-id="<?= $item->id ?>" data-target="#changeModel">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" data-id="<?= $item->id ?>"
                                        class="btn btn-danger delete-address"
                                >
                                    <i class="material-icons">close</i>
                                </button>
                            </td>
                        </tr>

                    <?php endforeach;
                } else{?>
                    <h6>Добавьте новый адрес</h6>
                <?php } ?>
                </tbody>
            </table>
            <h4>Варианты оплаты</h4>
            <div class="radio">
                <label>
                    <input type="radio" value="3000" name="radioAddress" class="checkOffice-2 address-cursor"><span class="circle"></span><span class="check"></span>
                    Оплатить картой и создать бронирование
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="4000" name="radioAddress" class="checkOffice address-cursor"><span class="circle"></span><span class="check"></span>
                    Создать бронирование (с оплатой на месте)
                </label>
            </div>

            <div id="modal">
                <button type="button" class="btn btn-info add-address" data-toggle="modal" data-target="#loginModal">Добавить новый адрес
                </button>
                <button type="submit" class="btn btn-default btn-yandex btn-or-guest" disabled="">Продолжить бронировать</button>
            </div>
            </form>
        </div>

    </div>
</div>


<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Добавить новый адрес</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'addAddress'
                    ]); ?>
                    <div class="card-content">
                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">location_city</i>
								</span>
                            <?= $form->field($newAddress, 'city')->textInput(['class' => 'form-control', 'placeholder' => 'Город'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">location_on</i>
								</span>
                            <?= $form->field($newAddress, 'address')->textInput(['class' => 'form-control', 'placeholder' => 'Адресс'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">home</i>
								</span>
                            <?= $form->field($newAddress, 'dom')->textInput(['class' => 'form-control', 'placeholder' => 'Дом'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="modal-footer text-center">
                            <button type="submit" href="#pablo" class="btn btn-primary btn-simple btn-wd btn-lg">
                                Добавить
                            </button>
                        </div>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="changeModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Изменить адрес</h4>
                    </div>
                </div>
                <div class="modal-body" id="changeAddress">

                </div>

            </div>
        </div>
    </div>
</div>
