<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 20.12.2018
 * Time: 1:34
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Бронировать';
?>
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">

        <div class="col-md-10 col-md-offset-1" style="margin-top: 20px">
            <?php if(!empty($contract)):?>
            <div class="table-responsive">
                <table class="table table-shopping">
                    <thead>
                    <tr>
                        <th>Устройство</th>
                        <th>Дата начала</th>
                        <th>Плановая дата окончания</th>
                        <th>Дней</th>
                        <th>Цена за один день</th>
                        <th>Количество устройств</th>
                        <th>Стоимость</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $product->name ?>
                        </td>
                        <td>
                            <?= $contract['date_begin'] ?>
                        </td>
                        <td>
                            <?= $contract['date_end'] ?>
                        </td>
                        <td>
                            <?= $contract['days'] ?>
                        </td>
                        <td>
                            <?= number_format($contract['one_day'], '2', '.', ' ') ?> руб.
                        </td>
                        <td>
                            <input type="text" id="count" style="width: 30px; display: inline-block"
                                   value="<?= $contract['count'] ?>" class="form-control">
                            <div class="btn-group">
                                <button class="btn btn-round btn-info btn-xs btn-minus-order"><i class="material-icons">remove</i>
                                </button>
                                <button class="btn btn-round btn-info btn-xs btn-plus-order"><i class="material-icons">add</i>
                                </button>
                            </div>
                        </td>
                        <td class="order-amount">
                            <?= number_format($contract['amount'], '0', ' ', ' ') ?> руб.
                        </td>
                    </tr>

                    </tbody>
                </table>
                <div class="col-md-8 col-md-offset-2">
                    <?php if (Yii::$app->user->isGuest): ?>
                        <a class="btn btn-primary" href="javascript:void (0)" data-toggle="modal"
                           data-target="#loginModal">Авторизоватся</a>
                        <a class="btn btn-info" href="<?= Url::to(['/site/guest']) ?>">Продолжить как гость</a>
                    <?php else: ?>
                        <a class="btn btn-info" href="<?= Url::to(['/site/ordering']) ?>">Продолжить</a>
                    <?php endif; ?>
                </div>
            </div>
            <?php else:?>
                <div style="min-height: 400px">
                    <h2>Корзинка пусто</h2>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
