<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
?>

<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="padding-top: 30px; min-height: 400px">
            <div class="abou-content">
                <h2 style="text-align: center"><?= $about->title ?></h2>
                <p><?= $about->content ?>

                </p>

                <input id="lat" class="hidden" value="<?= $about->lat; ?>"/>
                <input id="lng" class="hidden" value="<?= $about->lng; ?>"/>
                <?php if (!empty($socials)) {
                    foreach ($socials as $item):?>
                    <a class="btn btn-just-icon btn-facebook" href="<?= $item->url ?>">
                        <i class="fa <?= $item->icon ?>"></i>
                    </a>
                    <?php endforeach;
                } ?>
                <h4>Точка на карте</h4>
                <div class="field" id="map2" style="height: 300px; width: 100%; margin-top: 30px">
                    <script>


                        function initMap() {
                            var lat1 = Number(document.getElementById("lat").value);
                            var lng1 = Number(document.getElementById("lng").value);
                            var uluru = {lat: lat1, lng: lng1};
                            var map = new google.maps.Map(document.getElementById('map2'), {
                                zoom: 15,
                                center: uluru

                            });
                            var marker = new google.maps.Marker({

                                position: uluru,
                                map: map
                            });
                        }


                    </script>
                    <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                    </script>

                </div>

            </div>

            <div class="section container text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title"><?= $advantages->name ?></h2>
                        <h5 class="description"><?= $advantages->content ?></h5>
                    </div>
                </div>

                <div class="features">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info">
                                <div class="icon icon-info">
                                    <img class="adv-img" src="/uploads/<?= $advantages->icon1 ?>">
                                </div>
                                <h4 class="info-title"><?= $advantages->title1 ?></h4>
                                <p><?= $advantages->content1 ?></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="info">
                                <div class="icon icon-success">
                                    <img class="adv-img" src="/uploads/<?= $advantages->icon2 ?>">
                                </div>
                                <h4 class="info-title"><?= $advantages->title2 ?></h4>
                                <p><?= $advantages->content2 ?></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="info">
                                <div class="icon icon-success">
                                    <img class="adv-img" src="/uploads/<?= $advantages->icon3 ?>">
                                </div>
                                <h4 class="info-title"><?= $advantages->title3 ?></h4>
                                <p><?= $advantages->content3 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
