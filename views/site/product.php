<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 29.12.2018
 * Time: 18:48
 */

use app\models\Markers;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Продукты';

?>

<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="padding-top: 30px; min-height: 400px">
            <div class="row">
                <?php if (!empty($product)): ?>
                    <div class="col-md-4">
                        <div class="product-img">
                            <img src="/uploads/<?= $product->image ?>">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="product-content">
                            <h3><?= $product->title ?></h3>
                            <p><?= $product->content ?></p>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card card-form-horizontal card-order margintb-20">
                <div class="card-content order-container">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['/site/order'])
                    ]) ?>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= $form->field($model, 'date_begin')->widget(DatePicker::className(), [
                                    'name' => 'dp_22',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'pluginOptions' => [
                                        'class' => 'as',
                                        'autoclose' => true,
                                        'todayHighlight' => true,
                                        'minDate' => 0,
                                        'startDate' => date('d.m.Y'),
                                        'format' => 'dd.mm.yyyy'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
                                    'name' => 'dp_22',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'todayHighlight' => true,
                                        'minDate' => 0,
                                        'startDate' => date('d.m.Y', time() + 24 * 60 * 60),
                                        'format' => 'dd.mm.yyyy'
                                    ]
                                ])->label('Плановая дата окончания') ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= $form->field($model, 'marker_id')->dropDownList(ArrayHelper::map(
                                    Markers::find()->all(), 'id', 'address'
                                ), [
                                    'class' => 'form-control',
                                    'prompt' => 'Выберите адрес',
                                    'onChange' => 'showMap()'
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-lg-4 col-md-6">
                                <?= $form->field($model, 'count')->textInput(['id' => 'count', 'value' => 1])->label('Количество') ?>
                            </div>
                            <div class="col-lg-8 col-md-6">
                                <div class="btn-group mt60">
                                    <button class="btn btn-round btn-info btn-xs btn-minus"><i
                                                class="material-icons">remove</i>
                                    </button>
                                    <button class="btn btn-round btn-info btn-xs btn-plus"><i
                                                class="material-icons">add</i>
                                    </button>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-2 btn-margin">
                            <div class="form-group btn-margin">
                                <button type="submit" class="btn btn-primary btn-block">Забронировать</button>
                            </div>
                        </div>

                    </div>
                    <?php ActiveForm::end() ?>
                    <input id="lat" class="hidden" value=""/>
                    <input id="lng" class="hidden" value=""/>
                    <div class="mapsColl">

                        <div class="main-map field hidden" id="map2">
                            <script>
                                function initMap() {
                                    var lat1 = Number(document.getElementById("lat").value);
                                    var lng1 = Number(document.getElementById("lng").value);
                                    var uluru = {lat: lat1, lng: lng1};
                                    var map = new google.maps.Map(document.getElementById('map2'), {
                                        zoom: 15,
                                        center: uluru
                                    });
                                    var marker = new google.maps.Marker({
                                        position: uluru,
                                        map: map
                                    });
                                }
                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                            </script>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row faq-content" style="margin-top: 30px">
            <div class="row">
                <h2 style="text-align: center">Тарифы</h2>
                <div class="col-md-12">
                    <div class="row">
                        <?php if (!empty($tariff)) {
                            foreach ($tariff as $item): ?>
                                <div class="col-md-4">
                                    <div class="card card-pricing <?= $item->status == 1 ? 'card-raised' : 'card-plain' ?>">
                                        <div class="card-content <?= $item->status == 1 ? 'content-rose' : '' ?>">
                                            <h6 class="category text-info"><?= $item->name ?></h6>
                                            <h3 class="card-title"><?= $item->price ?></h3>
                                            <ul>
                                                <li><?= $item->speed ?></li>
                                                <li><?= $item->app ?></li>
                                                <li><?= $item->battery ?></li>
                                                <li><?= $item->extra ?></li>
                                                <li><?= $item->calls ?></li>
                                                <li><?= $item->hotspot ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;
                        } ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

