<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header header-filter"
     style="background-image: url('/images/bg2.jpg'); background-size: cover; background-position: top center; min-height: 800px">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="card card-signup">

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Регистрация</h4>
                    </div>
                    <?php $formSign = ActiveForm::begin() ?>
                    <div class="card-content">

                        <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">email</i>
										</span>
                            <?= $formSign->field($user, 'email')->textInput(['type' => 'email', 'placeholder' => 'Email'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
                            <div class="form-group is-empty">
                                <?= $formSign->field($user, 'password')->textInput(['type' => 'password', 'placeholder' => 'Пароль'])->label(false) ?>
                                <span class="material-input"></span></div>
                        </div>
                        <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">account_circle</i>
										</span>
                            <div class="form-group is-empty">
                                <?= $formSign->field($model, 'fio')->textInput(['type' => 'text', 'placeholder' => 'ФИО'])->label(false) ?>
                                <span class="material-input"></span></div>
                        </div>
                        <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">phone</i>
										</span>
                            <div class="form-group is-empty">
                                <?= $formSign->field($model, 'phone')->textInput(['type' => 'text', 'placeholder' => 'Телефон номер'])->label(false) ?>
                                <span class="material-input"></span></div>
                        </div>
                    </div>


                    <div style="text-align: center; padding-left: 20px">
                        <button class="btn btn-primary btn-simple btn-wd btn-lg" type="submit">Регистрация</button>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>

</div>