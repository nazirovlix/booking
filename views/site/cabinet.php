<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 21.12.2018
 * Time: 14:05
 */

use yii\widgets\ActiveForm;
$this->title = 'Кабинет';
?>
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="padding-top: 30px; min-height: 400px">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="profile-tabs">
                        <div class="nav-align-center">
                            <ul class="nav nav-pills nav-pills-icons" role="tablist">

                                <li class="active">
                                    <a href="#connections" role="tab" data-toggle="tab" aria-expanded="false">
                                        <i class="material-icons">list</i>
                                        Бронирования
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#work" role="tab" data-toggle="tab" aria-expanded="true">
                                        <i class="material-icons">assignment_ind</i>
                                        Профиль
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#media" role="tab" data-toggle="tab" aria-expanded="false">
                                        <i class="material-icons">location_on</i>
                                        Адреса
                                    </a>
                                </li>
                            </ul>


                        </div>
                    </div>
                    <!-- End Profile Tabs -->
                </div>
            </div>
            <div class="tab-content">

                <div class="tab-pane connections active" id="connections">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Номер договора</th>
                                        <th>Дата начала</th>
                                        <th>дата окончания</th>
                                        <th>Сумма договора</th>
                                        <th>Тип оплаты</th>
                                        <th>Статус договора</th>
                                        <th>Просрочен</th>
                                        <th>Скачать договор</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (!empty($contracts)) {
                                        foreach ($contracts as $k => $item):?>
                                            <tr>
                                                <td class="text-center"><?= ++$k ?></td>
                                                <td><?= $item->contract_id ?></td>
                                                <td><?= $item->date_begin ?></td>
                                                <td><?= $item->date_end ?></td>
                                                <td><?= number_format($item->amount, '0', ' ', ' ') ?> руб.</td>
                                                <td><?php if ($item->payed_status == 1) {
                                                        echo 'Наличные (в офисе)';
                                                    } else {
                                                        echo 'Дебетовая карта';
                                                    } ?></td>
                                                <td><?php if ($item->status == 1) {
                                                        echo 'Действующий';
                                                    } elseif ($item->status == 2) {
                                                        echo 'Будущий';
                                                    } else {
                                                        echo 'Закрытый';
                                                    }
                                                    ?></td>
                                                <td><?= !empty($item->contract_pdf) ? 'Да' : "Нет" ?></td>
                                                <td><?php $pdf = !empty($item->contract_pdf) ? '<a href="/uploads/pdf/' . $item->contract_pdf . '" download>PDF_2</a>' : ''; ?>
                                                    <a href="/uploads/pdf/<?= $item->id . '-' . $item->contract_id ?>.pdf"
                                                       class="btn btn-success" download>Скачать PDF</a>
                                                    <?php
                                                    if (!empty($item->contract_pdf)) {
                                                        echo '<a href="/uploads/pdf/' . $item->contract_pdf . '" class="btn btn-info" download>Скачать PDF_2</a>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane work" id="work">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-1">
                            <h4 class="title">Данные пользователя</h4>
                            <?php if (Yii::$app->session->hasFlash('cabinet')): ?>
                                <div class="alert alert-success">
                                    <div class="container">
                                        <div class="alert-icon">
                                            <i class="material-icons">check</i>
                                        </div>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <b>Успешно!</b> Ваши данные успешно сохранены.
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="row collections">
                                <?php $formSign = ActiveForm::begin() ?>
                                <div class="card-content">

                                    <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">email</i>
										</span>
                                        <?= $formSign->field($user, 'email')->textInput(['type' => 'email', 'placeholder' => 'Email', 'disabled' => 'disabled'])->label(false) ?>
                                        <span class="material-input"></span>
                                    </div>
                                    <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
                                        <div class="form-group is-empty">
                                            <?= $formSign->field($user, 'old_password')->textInput(['type' => 'password', 'placeholder' => 'Пароль'])->label(false) ?>
                                            <span class="material-input"></span></div>
                                    </div>
                                    <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">perm_identity</i>
										</span>
                                        <div class="form-group is-empty">
                                            <?= $formSign->field($model, 'fio')->textInput(['type' => 'text', 'placeholder' => 'ФИО'])->label(false) ?>
                                            <span class="material-input"></span></div>
                                    </div>
                                    <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">picture_in_picture</i>
										</span>
                                        <div class="form-group is-empty">
                                            <?= $formSign->field($model, 'possport_n')->textInput(['type' => 'text', 'placeholder' => 'Паспорт №'])->label(false) ?>
                                            <span class="material-input"></span></div>
                                    </div>
                                    <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">phone</i>
										</span>
                                        <div class="form-group is-empty">
                                            <?= $formSign->field($model, 'phone')->textInput(['type' => 'text', 'placeholder' => 'Телефон номер'])->label(false) ?>
                                            <span class="material-input"></span></div>
                                    </div>
                                </div>


                                <div style="text-align: center; padding-left: 20px">
                                    <button class="btn btn-primary btn-simple btn-wd btn-lg" type="submit">Сохранить
                                    </button>
                                </div>
                                <?php ActiveForm::end() ?>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane text-center" id="media">
                    <div class="row" id="modals">


                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Город</th>
                                    <th>Адрес</th>
                                    <th>Дом</th>

                                </tr>
                                </thead>
                                <tbody id="addresses">
                                <?php if (!empty($address)) {
                                    foreach ($address as $k => $item): ?>
                                        <tr>
                                            <td class="text-center"><?= ++$k ?></td>
                                            <td class="text-left"><?= $item->city ?></td>
                                            <td class="text-left"><?= $item->address ?></td>
                                            <td class="text-left"><?= $item->dom ?></td>
                                            <td class="td-actions text-right">
                                                <button type="button" rel="tooltip"
                                                        class="btn btn-success change-address" data-toggle="modal"
                                                        data-id="<?= $item->id ?>" data-target="#changeModel">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                <button type="button" rel="tooltip" data-id="<?= $item->id ?>"
                                                        class="btn btn-danger delete-address"
                                                        >
                                                    <i class="material-icons">close</i>
                                                </button>
                                            </td>
                                        </tr>

                                    <?php endforeach;
                                } ?>
                                </tbody>
                            </table>
                            <div id="modal">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#loginModal">Добавить
                                    новый
                                    адрес
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Добавить новый адрес</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                            'id' => 'addAddress'
                    ]); ?>
                    <div class="card-content">
                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">location_city</i>
								</span>
                            <?= $form->field($newAddress, 'city')->textInput(['class' => 'form-control', 'placeholder' => 'Город'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">location_on</i>
								</span>
                            <?= $form->field($newAddress, 'address')->textInput(['class' => 'form-control', 'placeholder' => 'Адресс'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">home</i>
								</span>
                            <?= $form->field($newAddress, 'dom')->textInput(['class' => 'form-control', 'placeholder' => 'Дом'])->label(false) ?>
                            <span class="material-input"></span>
                        </div>
                        <div class="modal-footer text-center">
                            <button type="submit" href="#pablo" class="btn btn-primary btn-simple btn-wd btn-lg">
                                Добавить
                            </button>
                        </div>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="changeModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Изменить адрес</h4>
                    </div>
                </div>
                <div class="modal-body" id="changeAddress">

                </div>

            </div>
        </div>
    </div>
</div>
<?php if (Yii::$app->session->hasFlash('create-contract')): ?>
    <?php $this->registerJs('swal("Спасибо за заказ!","Ваше бронирование принято.","success")') ?>
<?php endif; ?>