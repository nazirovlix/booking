<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 21.12.2018
 * Time: 1:46
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header header-filter"
     style="background-image: url('/images/bg2.jpg'); background-size: cover; background-position: top center; min-height: 800px">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="card card-signup">
                      <h4>Сообщение отправлено, Пожалуйста, подтвердите свой почтовый адрес.</h4>

                </div>
            </div>
        </div>
    </div>
</div>