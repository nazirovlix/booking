<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 23.12.2018
 * Time: 0:39
 */

?>
<?php if (Yii::$app->session->hasFlash('addressChanged')): ?>
    <?php $this->registerJs('swal("Успешно!","Ваши данные успешно сохранены.","success")') ?>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('deleted')): ?>
    <?php $this->registerJs('swal("Успешно!","Ваш адрес удален","success")') ?>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('newAddress')): ?>
    <?php $this->registerJs('swal("Успешно!","Новый адрес добавлен.","success")') ?>
<?php endif; ?>
<?php if (!empty($address)) {
    foreach ($address as $k => $item): ?>
        <tr class="address-cursor addr">
            <td>
                <div class="radio">
                    <label>
                        <input type="radio" name="radio-address" class="check-address"><span class="circle"></span><span class="check"></span>
                    </label>
                </div>
            </td>
            <td class="text-center"><?= ++$k ?></td>
            <td class="text-left"><?= $item->city ?></td>
            <td class="text-left"><?= $item->address ?></td>
            <td class="text-left"><?= $item->dom ?></td>
            <td class="td-actions text-right">
                <button type="button" rel="tooltip" class="btn btn-success change-address" data-toggle="modal" data-id="<?= $item->id?>" data-target="#changeModel">
                    <i class="material-icons">edit</i>
                </button>
                <button type="button" rel="tooltip" data-id="<?= $item->id ?>"
                        class="btn btn-danger delete-address"
                        >
                    <i class="material-icons">close</i>
                </button>
            </td>
        </tr>

    <?php endforeach;
} ?>
