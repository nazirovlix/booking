<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 23.12.2018
 * Time: 0:29
 */

use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'id' => 'changeForm'
]); ?>
<div class="card-content">
   <input name="id" value="<?= $newAddress->id ?>" type="hidden">
    <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">location_city</i>
								</span>
        <?= $form->field($newAddress, 'city')->textInput(['class' => 'form-control', 'placeholder' => 'Город'])->label(false) ?>
        <span class="material-input"></span>
    </div>
    <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">location_on</i>
								</span>
        <?= $form->field($newAddress, 'address')->textInput(['class' => 'form-control', 'placeholder' => 'Адресс'])->label(false) ?>
        <span class="material-input"></span>
    </div>
    <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">home</i>
								</span>
        <?= $form->field($newAddress, 'dom')->textInput(['class' => 'form-control', 'placeholder' => 'Дом'])->label(false) ?>
        <span class="material-input"></span>
    </div>

    <div class="modal-footer text-center">
        <button type="submit" href="#pablo" class="btn btn-primary btn-simple btn-wd btn-lg">Изменить
        </button>
    </div>
</div>
<?php ActiveForm::end() ?>
