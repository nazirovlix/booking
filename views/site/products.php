<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 29.12.2018
 * Time: 18:48
 */

use app\models\Markers;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Продукты';

?>

<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('/img/bg9.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">Booking</h1>
                <h4>АРЕНДА МОБИЛЬНОГО HOTSPOT</h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="padding-top: 30px; min-height: 700px">
            <div class="row">
                <h3>Продукты</h3>
                <?php if (!empty($product)): ?>

                    <div class="col-md-3">
                        <div class="product-img">
                            <img src="/uploads/<?= $product->image ?>">
                        </div>
                        <div style="display: block; text-align: center">

                        <h4><a href="<?= Url::to(['/site/product','id' => $product->id]) ?>"> <?= $product->title ?> </a></h4>
                        </div>
                    </div>

                <?php endif; ?>
            </div>

        </div>
    </div>
</div>

