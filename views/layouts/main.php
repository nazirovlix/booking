<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;
use app\models\forms\Login;

$login = new Login();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="about-us">
<?php $this->beginBody() ?>

<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Booking</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?= Url::to(['/site/products']) ?>">
                         Продукты
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/site/faq']) ?>">
                        F.A.Q
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/site/contact']) ?>">
                       Контакты
                    </a>
                </li>
                <?php if(Yii::$app->user->isGuest): ?>
                    <li>
                        <a href="<?= Url::to(['/site/login']) ?>">
                            <i class="material-icons">account_circle</i>
                            Вход
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/site/signup']) ?>">
                            <i class="material-icons">assignment</i>
                            Регистрация
                        </a>
                    </li>
                <?php else:?>
                <li>
                    <a href="<?= Url::to(['/site/cabinet']) ?>">
                        <i class="material-icons">account_circle</i>
                        <?php
                        $user = User::findOne(Yii::$app->user->id);
                        echo $user->username;
                        ?>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/site/logout']) ?>" data-method="post">
                        <i class="material-icons">reply</i>
                        Выйти
                    </a>
                </li>
                <?php endif; ?>
                <li>
                    <a href="<?= Url::to(['/site/order']) ?>">
                        <i class="material-icons">local_grocery_store</i>
                       Корзина
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>

        <?= $content ?>


<footer class="footer">
    <div class="container">

        <div class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script>
        </div>
    </div>
</footer>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Авторизация</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['/site/login'])
                    ]) ?>
                    <div class="card-content">

                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">face</i>
								</span>
                            <div class="form-group is-empty">
                                <?= $form->field($login, 'username')->textInput(['placeholder' => "Email"])->label(false) ?>
                                <span class="material-input"></span>
                            </div>
                        </div>

                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">lock_outline</i>
								</span>
                            <div class="form-group is-empty">
                                <?= $form->field($login, 'password')->passwordInput(['placeholder' => "Пароль"])->label(false) ?>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center; padding-left: 20px">
                        <button class="btn btn-primary btn-wd btn-lg" type="submit">Войти</button>
                    </div>
                    <div>

                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
