<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\About */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Abouts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',

            'title',
            'content:ntext',
        ],
    ]) ?>
    <input id="lat" class="hidden" value="<?= $model->lat; ?>"/>
    <input id="lng" class="hidden" value="<?= $model->lng; ?>"/>

    <h4>Точка на карте</h4>
    <div class="field" id="map2" style="height: 200px; width: 100%; margin-top: 30px">
        <script>


            function initMap() {
                var lat1 = Number(document.getElementById("lat").value);
                var lng1 = Number(document.getElementById("lng").value);
                var uluru = {lat: lat1, lng: lng1};
                var map = new google.maps.Map(document.getElementById('map2'), {
                    zoom: 15,
                    center: uluru

                });
                var marker = new google.maps.Marker({

                    position: uluru,
                    map: map
                });
            }


        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
        </script>

    </div>

</div>
