<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Profiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User Profile'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Ближайшие дни рождения '), ['bdays'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'user_id',
            'fio',
            'possport_n',
            'phone',
            'bday:date',
            [
                'attribute' => 'blocked',
                'value' => function ($model) {
                    return $model->blocked == 0 ? '<p style="color:#0b58a2">Не блокирован</p>' : '<p style="color:#f00">Блокирован</p>';
                },
                'format' => 'html',
                'filter' => [
                    '0' => 'Не блокирован',
                    '1' => 'Блокирован',
                ]
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
