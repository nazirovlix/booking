<?php

use app\models\Markers;
use app\models\Worker;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($user, 'email')->textInput()->label('Почта') ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($user, 'password')->passwordInput()->label('Пароль') ?>
        </div>
        <div class="col-6">
            <?= $form->field($user, 'retypePassword')->passwordInput()->label('Повторите пороль') ?>
        </div>
    </div>
    <hr>
    <h4>Информация о клиенте</h4>
    <div class="row">
        <div class="col-6"><?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?></div>
        <div class="col-6"><?= $form->field($model, 'possport_n')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
        <div class="col-6"><?= $form->field($model, 'bday')->widget(DatePicker::className(), [
                'name' => 'dp_2',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,

                'pluginOptions' => [
                    'defaultViewDate' => '01.01.1990',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',

                ]
            ]) ?></div>
    </div>

    <?php if (!Yii::$app->user->can('admin')): ?>
        <?php $worker = Worker::findOne(['user_id' => Yii::$app->user->id]) ?>
        <?php if ($worker): ?>
            <?= $form->field($model, 'marker_id')->dropDownList(
                ArrayHelper::map(Markers::find()->where(['id' => $worker->marker_id])->all(), 'id', 'address'))
            ?>
        <?php endif; ?>
    <?php else: ?>
        <?= $form->field($model, 'marker_id')->dropDownList(
            ArrayHelper::map(Markers::find()->all(), 'id', 'address'))
        ?>
    <?php endif; ?>

    <?= $form->field($model, 'blocked')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
