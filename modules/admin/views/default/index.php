<!-- Start Page Content -->
<div class="row">
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-ruble f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($amountContracts,'0',' ',' ')?></h2>
                    <p class="m-b-0">Сумма договоров</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($countContracts,'0',' ',' ')?></h2>
                    <p class="m-b-0">Количество договоров</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-map-marker f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $markers ?></h2>
                    <p class="m-b-0">Точки</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $clients ?></h2>
                    <p class="m-b-0">Клиенты</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card" style="min-height: 400px">
            <div class="card-title">
                <h4>Последные договоры </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Клиент</th>
                            <th>Устройство</th>
                            <th>Дата начала</th>
                            <th>Дата окончания</th>
                            <th>Сумма</th>
                            <th>Статус оплаты</th>
                            <th>Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($contracts)): ?>
                            <?php foreach ($contracts as $item):?>
                                <tr>
                                    <td><a href="<?= \yii\helpers\Url::to(['/admin/contracts/view','id' => $item->id]) ?>"><?= $item->id ?></a></td>
                                    <td><a href="<?= \yii\helpers\Url::to(['/admin/user-profile/view','id' => $item->user_id]) ?>"><?= $item->user->fio ?></a></td>
                                    <td><a href="<?= \yii\helpers\Url::to(['/admin/products/view','id' => $item->product_id]) ?>"><?= $item->product->name ?></a></td>
                                    <td><?= $item->date_begin ?></td>
                                    <td><?= $item->date_end ?></td>
                                    <td><?= number_format($item->amount,'0',' ',' ')?> руб</td>
                                    <td><?php

                                        if ($item->payed_status == 1) {
                                            echo 'Наличные (в офисе)';
                                        } else {
                                            echo 'Карта';
                                        }
                                        ?>
                                    </td>
                                    <td><?php
                                        if ($item->status == 1) {
                                            echo 'Действующий';
                                        } elseif ($item->status == 2) {
                                            echo 'Будущий';
                                        } else {
                                            echo 'Закрытый';
                                        }
                                    ?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End PAge Content -->