<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AdvantagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advantages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'icon1') ?>

    <?= $form->field($model, 'title1') ?>

    <?php // echo $form->field($model, 'content1') ?>

    <?php // echo $form->field($model, 'icon2') ?>

    <?php // echo $form->field($model, 'title2') ?>

    <?php // echo $form->field($model, 'content2') ?>

    <?php // echo $form->field($model, 'icon3') ?>

    <?php // echo $form->field($model, 'title3') ?>

    <?php // echo $form->field($model, 'content3') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
