<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Advantages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advantages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'image1')->widget(
                Upload::className(),
                [
                    'url' => ['/admin/file-storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

                ]);
            ?>
        </div>
        <div class="col-9">
            <?= $form->field($model, 'title1')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content1')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'image2')->widget(
                Upload::className(),
                [
                    'url' => ['/admin/file-storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

                ]);
            ?>
        </div>
        <div class="col-9">
            <?= $form->field($model, 'title2')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content2')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'image3')->widget(
                Upload::className(),
                [
                    'url' => ['/admin/file-storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

                ]);
            ?>
        </div>
        <div class="col-9">
            <?= $form->field($model, 'title3')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content3')->textarea(['rows' => 6]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
