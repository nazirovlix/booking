<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Advantages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Advantages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advantages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'content:ntext',
            [
                'attribute' => 'image1',
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="/uploads/' . $model->icon1 . '" style="width:50px">';
                }
            ],
            'title1',
            'content1:ntext',
            [
                'attribute' => 'image2',
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="/uploads/' . $model->icon2 . '" style="width:50px">';
                }
            ],
            'title2',
            'content2:ntext',
            [
                'attribute' => 'image3',
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="/uploads/' . $model->icon3 . '" style="width:50px">';
                }
            ],
            'title3',
            'content3:ntext',
        ],
    ]) ?>

</div>
