<?php
/**
 * Created by PhpStorm.
 * User: Hasanjon
 */

use app\models\User;
use app\models\Worker;
use app\modules\admin\widgets\Menu;
use yii\helpers\Url;


?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php if (Yii::$app->user->can('admin')): ?>
                <?php
                try {
                    echo Menu::widget([
                        'options' => ['id' => 'sidebarnav'],
                        'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                        'badgeClass' => 'label label-rouded label-primary pull-right',
                        'activateParents' => true,
                        'items' => [
                            [
                                'label' => '',
                                'options' => ['class' => 'nav-devider']
                            ],
                            [
                                'label' => 'Главная',
                                'url' => ['default/index'],
                                'icon' => '<i class="fa fa-tachometer"></i>',
                            ],
                            [
                                'label' => 'APP',
                                'options' => ['class' => 'nav-label']
                            ],
                            [
                                'label' => 'Точки',
                                'url' => Url::to(['markers/index']),
                                'icon' => '<i class="fa fa-map-marker"></i>',
                            ],
                            [
                                'label' => 'Сотрудники',
                                'url' => '#',
                                'icon' => '<i class="fa fa-suitcase"></i>',
                                'items' => [
                                    [
                                        'label' => 'Сотрудники',
                                        'url' => Url::to(['worker/index']),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Должности',
                                        'url' => Url::to(['positions/index']),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Устройства',
                                'url' => Url::to(['products/index']),
                                'icon' => '<i class="fa fa-code-fork"></i>',
                            ],
                            [
                                'label' => 'Клиенты',
                                'url' => Url::to(['user-profile/index']),
                                'icon' => '<i class="fa fa-group"></i>',
                            ],
                            [
                                'label' => 'Договор',
                                'url' => Url::to(['contracts/index']),
                                'icon' => '<i class="fa fa-file-text-o"></i>',
                            ],
                            [
                                'label' => 'Тарифы',
                                'url' => Url::to(['tarifs/index']),
                                'icon' => '<i class="fa fa-rub"></i>',
                            ],
                            [
                                'label' => 'Карты связи',
                                'url' => Url::to(['card/index']),
                                'icon' => '<i class="fa fa-credit-card"></i>',
                            ],
                            [
                                'label' => 'Данные сайта',
                                'options' => ['class' => 'nav-label']
                            ],
                            [
                                'label' => 'О нас',
                                'url' => '#',
                                'icon' => '<i class="fa fa-align-justify"></i>',
                                'items' => [
                                    [
                                        'label' => 'О нас',
                                        'url' => Url::to(['about/view', 'id' => 1]),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Преимущества',
                                        'url' => Url::to(['advantages/view', 'id' => 1]),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Социальные ссылки',
                                        'url' => Url::to(['socials/index']),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Страница тарифы',
                                'url' => Url::to(['tariff/index']),
                                'icon' => '<i class="fa fa-rub"></i>',
                            ],

                            [
                                'label' => 'F.A.Q',
                                'url' => '#',
                                'icon' => '<i class="fa fa-question"></i>',
                                'items' => [
                                    [
                                        'label' => 'Категории',
                                        'url' => Url::to(['faq-category/index']),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'FAQs',
                                        'url' => Url::to(['faqs/index']),
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                ],
                            ],
//                            [
//                                'label' => 'Блоги',
//                                'url' => Url::to(['blogs/index']),
//                                'icon' => '<i class="fa fa-th-large"></i>',
//                            ],
//
//                            [
//                                'label' => 'Советы',
//                                'url' => Url::to(['tips/index']),
//                                'icon' => '<i class="fa fa-comments"></i>',
//                            ],
                            [
                                'label' => 'Страница устройств',
                                'url' => Url::to(['product-page/view', 'id' => 1]),
                                'icon' => '<i class="fa fa-th-large"></i>',
                            ],
                        ]
                    ]);
                } catch (Exception $e) {
                }
                ?>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('director')): ?>
                <?php $worker = Worker::findOne(['user_id' => Yii::$app->user->id]); ?>
                <?php
                try {
                    echo Menu::widget([
                        'options' => ['id' => 'sidebarnav'],
                        'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                        'badgeClass' => 'label label-rouded label-primary pull-right',
                        'activateParents' => true,
                        'items' => [
                            [
                                'label' => '',
                                'options' => ['class' => 'nav-devider']
                            ],
                            [
                                'label' => 'Главная',
                                'url' => ['default/index'],
                                'icon' => '<i class="fa fa-tachometer"></i>',
                            ],
                            [
                                'label' => 'APP',
                                'options' => ['class' => 'nav-label']
                            ],
                            [
                                'label' => 'Точка',
                                'url' => Url::to(['markers/view', 'id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-map-marker"></i>',
                            ],
                            [
                                'label' => 'Сотрудники',
                                'url' => Url::to(['worker/index', 'id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-suitcase"></i>',
                            ],
                            [
                                'label' => 'Устройства',
                                'url' => Url::to(['products/index', 'id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-code-fork"></i>',
                            ],
                            [
                                'label' => 'Клиенты',
                                'url' => Url::to(['user-profile/index','id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-group"></i>',
                            ],
                            [
                                'label' => 'Договор',
                                'url' => Url::to(['contracts/index', 'id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-file-text-o"></i>',
                            ],
                        ]
                    ]);
                } catch (Exception $e) {
                }
                ?>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('menedjer')): ?>

                <?php $worker = Worker::findOne(['user_id' => Yii::$app->user->id]); ?>

                <?php
                try {
                    echo Menu::widget([
                        'options' => ['id' => 'sidebarnav'],
                        'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                        'badgeClass' => 'label label-rouded label-primary pull-right',
                        'activateParents' => true,
                        'items' => [
                            [
                                'label' => '',
                                'options' => ['class' => 'nav-devider']
                            ],
                            [
                                'label' => 'Главная',
                                'url' => ['default/index'],
                                'icon' => '<i class="fa fa-tachometer"></i>',
                            ],
                            [
                                'label' => 'APP',
                                'options' => ['class' => 'nav-label']
                            ],

                            [
                                'label' => 'Устройства',
                                'url' => Url::to(['products/index', 'id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-code-fork"></i>',
                            ],
                            [
                                'label' => 'Клиенты',
                                'url' => Url::to(['user-profile/index','id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-group"></i>',
                            ],
                            [
                                'label' => 'Договор',
                                'url' => Url::to(['contracts/index', 'id' => $worker->marker_id]),
                                'icon' => '<i class="fa fa-file-text-o"></i>',
                            ],
                        ]
                    ]);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                ?>
            <?php endif; ?>
        </nav>
    </div>
</div>
