<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FaqCategory */

$this->title = 'FAQ Категории';
$this->params['breadcrumbs'][] = ['label' => 'FAQ Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
