<?php

use app\models\Contracts;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
//            [
//                'attribute' => 'Фото',
//                'format' => 'html',
//                'value' => function ($model) {
//                    return '<img src="/uploads/' . $model->image . '" style="width:120px">';
//                }
//            ],
            'name',
            'serial_number',
            'extension_number',
            [
                'attribute' => 'free',
                'value' => function ($model) {
                    if($model->free == 1){
                        return '<p style="color:#0b58a2">Свободно</p>';
                    } else {
                        $contracts = Contracts::find()->where(['product_id' => $model->id])->one();
                        if(!empty($contracts)){
                            return '<p style="color:#f00">Занято (Дог № '. $contracts->contract_id.'</p>';
                        }
                        return '<p style="color:#f00">Сдан в аренду</p>';
                    }
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Свободно',
                    '0' => 'Сдан в аренду'
                ]
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    if ($model->status == 1) {
                        return 'Рабочее';
                    } elseif ($model->status == 2) {
                        return 'В ремонте';
                    } else {
                        return 'Списано';
                    }
                },
                'format' => 'html',
                'filter' => [
                    '0' => 'Списано',
                    '1' => 'Рабочее',
                    '2' => 'В ремонте',
                ]
            ],
            //'description:ntext',
            //'content:ntext',
            //'free',
            //'status',
            [
                'attribute' => 'marker_id',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/markers/view', 'id' => $model->marker_id]) . '">' . $model->marker_id . '<a/>';
                },
                'format' => 'html'
            ],

            //'created_at',
            //'updated_at',

            [ 'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
