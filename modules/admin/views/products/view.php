<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->name;
if (Yii::$app->user->can('admin')):
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
endif;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'serial_number',
            'extension_number',
//            [
//                'attribute' => 'Главное фото',
//                'format' => 'html',
//                'value' => function ($model) {
//                    return '<img src="/uploads/' . $model->image . '" style="width:150px">';
//                }
//            ],


            'barcode',
//            'description:ntext',
//            'content:ntext',
            [
                'attribute' => 'free',
                'value' => function ($model) {
                    return $model->free == 1 ? '<p style="color:#0b58a2">Свободно</p>' : '<p style="color:#f00">Сдан в аренду</p>';
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->status == 1 ? '<p style="color:#0b58a2">Опубликовно</p>' : '<p style="color:#f00">Не опубликовно</p>';
                },
                'format' => 'html',
            ],
            'marker_id',
            'marker.address',
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>

</div>
