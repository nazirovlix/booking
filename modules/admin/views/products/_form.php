<?php

use app\models\Markers;
use app\models\Worker;
use trntv\filekit\widget\Upload;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'extension_number')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'barcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'free')->dropDownList([
        '1' => 'Свободно',
        '0' => 'Сдан в аренду'
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Списано',
        '1' => 'Рабочее',
        '2' => 'В ремонте',
    ]) ?>

    <?php if (!Yii::$app->user->can('admin')): ?>
        <?php $worker = Worker::findOne(['user_id' => Yii::$app->user->id]) ?>
        <?php if ($worker): ?>
            <?= $form->field($model, 'marker_id')->dropDownList(
                ArrayHelper::map(Markers::find()->where(['id' => $worker->marker_id])->all(), 'id', 'address'))
            ?>
        <?php endif; ?>
    <?php else: ?>
        <?= $form->field($model, 'marker_id')->dropDownList(
            ArrayHelper::map(Markers::find()->all(), 'id', 'address'))
        ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
