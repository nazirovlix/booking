<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TipsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tips');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tips-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tips'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'Фото',
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="/uploads/' . $model->image . '" style="width:100px">';
                }
            ],
            'title',
            'description:ntext',
            //'content:ntext',
            //'meta_key',
            //'meta_descriptions:ntext',
            //'created_at',
            'updated_at:date',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    return $model->status == 1 ? '<p style="color:#0b58a2">Опубликовно</p>' : '<p style="color:#f00">Не опубликовно</p>';
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Опубликовно',
                    '0' => 'Не опубликовно'
                ]
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
