<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 04.12.2018
 * Time: 23:06
 */

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="contracts-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'date_begin')->widget(DatePicker::className(), [
                'name' => 'dp_22',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'minDate' => 0,
                    'startDate' => date('d.m.Y'),
                    'format' => 'dd.mm.yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
                'name' => 'dp_22',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'minDate' => 0,
                    'startDate' => date('d.m.Y',time()+24*60*60),
                    'format' => 'dd.mm.yyyy'
                ]
            ])->label('Плановая дата окончания') ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
