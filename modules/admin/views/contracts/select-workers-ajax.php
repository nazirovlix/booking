<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 27.11.2018
 * Time: 1:18
 */

use yii\helpers\Url;

?>

    <div class="col-6">
        <label class="">Поиск устройства</label>
        <input type="text" class="form-control productSelectInput"
               placeholder="Введите название или серийный номер">
    </div>
    <div class="col-6" id="productSelect">
        <div class="form-group field-contracts-product_id required">
            <label class="control-label" for="contracts-product_id">Устройства</label>
            <select id="contracts-product_id" class="form-control" name="Contracts[product_id]" aria-required="true" required="required">
                <option disabled>Выберите продукта</option>
                <?php if (!empty($products)): ?>
                    <?php foreach ($products as $item): ?>
                        <option value="<?= $item->id ?>"><?= $item->productInfo ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>

            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group field-contracts-worker_id required">
            <label class="control-label" for="contracts-worker_id">Менеджер</label>
            <select id="contracts-worker_id" class="form-control" name="Contracts[worker_id]" aria-required="true"
                    aria-invalid="true" required="required">
                <option disabled>Выберите сотрудника</option>
                <?php if (!empty($workers)): ?>
                    <?php foreach ($workers as $item): ?>
                        <option value="<?= $item->id ?>"><?= $item->fio ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>

            <div class="help-block">Необходимо заполнить «Worker ID».</div>
        </div>
    </div>

