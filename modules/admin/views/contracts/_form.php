<?php

use app\models\UserProfile;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contracts-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-5">
            <label class="">Клиент</label>
            <input type="text" class="form-control userSelect" data-url="<?= Url::to(['/admin/contracts/get-user']) ?>"
                   placeholder="Введите имя или номер паспорта">
        </div>
        <div id="userSelect" class="col-5">
            <?= $form->field($model, 'user_id')->dropDownList(
                ArrayHelper::map(UserProfile::find()
                    ->limit(10)
                    ->orderBy(['id' => SORT_DESC])
                    ->all(), 'id', 'fio'))
                ->label("Выберите клиента") ?>
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#mediumModal"
                    style="margin-top: 28px">Создать новый
            </button>
        </div>

    </div>

    <div style="margin-bottom: 14px">

        <label class="">Выберите точку</label>
        <select class="form-control selectMarker">
            <option disabled selected>Выберите точку</option>
            <?php if (!empty($markers)): ?>
                <?php foreach ($markers as $marker): ?>
                    <option value="<?= $marker->id ?>"
                        <?php if(!empty($markerId) && $markerId == $marker->id){
                             echo 'selected';
                        }
                        ?>><?= $marker->city ?> (<?= $marker->address ?>)</option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>

    </div>

    <div id="setWorker" class="row">
        <div class="col-6">
            <?= $form->field($model, 'worker_id')->dropDownList(
                ArrayHelper::map($dataWorker,'id','fio'),['prompt' => 'Выберите сотрудника']
            )->label('Менеджер') ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'product_id')->dropDownList(
                    ArrayHelper::map($dataProduct,'id','productInfo'),['prompt' => 'Выберите устройству']
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'date_begin')->widget(DatePicker::className(), [
                'name' => 'dp_22',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'minDate' => 0,
                    'startDate' => date('d.m.Y'),
                    'format' => 'dd.mm.yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
                'name' => 'dp_22',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'minDate' => 0,
                    'startDate' => date('d.m.Y',time()+24*60*60),
                    'format' => 'dd.mm.yyyy'
                ]
            ])->label('Плановая дата окончания') ?>
        </div>
    </div>
    <?= $form->field($model, 'card_number')->textInput() ?>

    <?= $form->field($model, 'payed_status')->dropDownList([
        '1' => 'Наличные (в офисе)',
        '2' => 'Дебетовая карта',
    ]) ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'status')->dropDownList([
                '1' => 'Действующий',
                '2' => 'Будущий,',
                '3' => 'Закрытый'
            ]) ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'overdue')->dropDownList([
                '0' => 'Не просроченный',
                '1' => 'Просроченный',
            ]) ?>
        </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
     </div>

    <?php ActiveForm::end(); ?>

<!-- MODAL CREATE CLIENT | USER  -->
    <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Создать пользователья</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'formSubmit',
                        'method' => 'get',
                        'action' => Url::to(['/admin/contracts/create-user'])
                    ]); ?>
                    <?= $form->field($user, 'username')->textInput()->label('Логин') ?>

                    <?= $form->field($user, 'email')->textInput()->label('Почта') ?>
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($user, 'password')->passwordInput()->label('Пароль') ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($user, 'retypePassword')->passwordInput()->label('Повторите пороль') ?>
                        </div>
                    </div>
                    <hr>
                    <h4>Информация о клиенте</h4>
                    <div class="row">
                        <div class="col-6"><?= $form->field($userProfile, 'fio')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-6"><?= $form->field($userProfile, 'possport_n')->textInput(['maxlength' => true]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-6"><?= $form->field($userProfile, 'phone')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-6"><?= $form->field($userProfile, 'bday')->widget(DatePicker::className(), [
                                'name' => 'dp_2',
                                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                //'value' => '23-Feb-1982',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy'
                                ]
                            ]) ?></div>
                    </div>

                    <?= $form->field($userProfile, 'blocked')->checkbox() ?>

                    <?php ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary submit-crete">Создать</button>
                </div>

            </div>
        </div>
    </div>

</div>
