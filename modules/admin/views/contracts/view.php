<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = '№ ' . $model->contract_id . ' , ' . $model->date_begin . ' - ' . $model->date_end;
if (Yii::$app->user->can('admin')):
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contracts'), 'url' => ['index']];
endif;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('admin')): ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Продлить договор'), ['extend', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'contract_id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/user-profile/view', 'id' => $model->user_id]) . '">' . $model->user->fio . '</a>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/products/view', 'id' => $model->product_id]) . '">' . $model->product->name . '</a>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'worker_id',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/worker/view', 'id' => $model->worker_id]) . '">' . $model->worker->fio . '</a>';
                },
                'format' => 'html'
            ],
            'date_begin:date',
            'date_end:date',
            [
                'attribute' => 'amount',
                'value' => function ($model) {
                    return number_format($model->amount, '0', ' ', ' ') . ' руб';
                }
            ],
            'card_number',

            [
                'attribute' => 'payed_status',
                'value' => function ($model) {
                    if ($model->payed_status == 1) {
                        return 'Наличные (в офисе)';
                    } else {
                        return 'Карта';
                    }
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Наличные (в офисе)',
                    '2' => 'Карта,',
                ]
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    if ($model->status == 1) {
                        return 'Действующий';
                    } elseif ($model->status == 2) {
                        return 'Будущий';
                    } else {
                        return 'Закрытый';
                    }
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'overdue',
                'value' => function ($model) {
                    if ($model->overdue == 1) {
                        return 'Да';
                    } else {
                        return 'Нет';
                    }
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'contract_pdf',
                'value' => function ($model) {
                    $pdf = !empty($model->contract_pdf) ? '<a href="/uploads/pdf/' . $model->contract_pdf . '" download>PDF_2</a>' : '';
                    return '<a href="/uploads/pdf/' . $model->id . '-' . $model->contract_id . '.pdf" download>PDF_1</a><br>' . $pdf;
                },
                'format' => 'raw'
            ],
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>

</div>
