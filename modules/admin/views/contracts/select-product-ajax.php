<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 03.12.2018
 * Time: 22:46
 */
?>

<div class="form-group field-contracts-product_id required">
    <label class="control-label" for="contracts-product_id">Устройства</label>
    <select id="contracts-product_id" class="form-control" name="Contracts[product_id]" aria-required="true" required="required">
        <option disabled>Выберите продукта</option>
        <?php if (!empty($products)): ?>
            <?php foreach ($products as $item): ?>
                <option value="<?= $item->id ?>"><?= $item->productInfo ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>

    <div class="help-block"></div>
</div>
