<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 26.11.2018
 * Time: 22:47
 */
?>

<div class="form-group field-contracts-user_id required">
    <label class="control-label" for="contracts-user_id">Выберите клиента</label>
    <select id="contracts-user_id" class="form-control" name="Contracts[user_id]" aria-required="true"
            aria-invalid="false" required="required">
        <?php if (!empty($users)): ?>
            <?php foreach ($users as $user): ?>
                <option value="<?= $user->user_id ?>"><?= $user->fio?> (Паспорт №<?= $user->possport_n ?>)</option>
            <?php endforeach; ?>
        <?php else: ?>
             <option disabled selected>Ничего не найдено</option>
        <?php endif; ?>
    </select>

    <div class="help-block"></div>
</div>