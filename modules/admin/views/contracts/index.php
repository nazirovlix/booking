<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ContractsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contracts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Contracts'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'contract_id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/user-profile/view', 'id' => $model->user_id]) . '">' . $model->user->fio . '</a>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'product_name',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/products/view', 'id' => $model->product_id]) . '">' . $model->product->name . '</a>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'worker_id',
                'value' => function ($model) {
                    return '<a href="' . Url::to(['/admin/worker/view', 'id' => $model->worker_id]) . '">' . $model->worker->fio . '</a>';
                },
                'format' => 'html'
            ],
            'date_begin:date',
            'date_end:date',
            [
                'attribute' => 'amount',
                'value' => function ($model) {
                    return number_format($model->amount, '0', ' ', ' ') . ' руб';
                }
            ],
            //'contract_pdf',
            [
                'attribute' => 'payed_status',
                'value' => function ($model) {
                    if ($model->payed_status == 1) {
                        return 'Наличные (в офисе)';
                    } else {
                        return 'Дебетовая карта';
                    }
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Наличные (в офисе)',
                    '2' => 'Дебетовая карта',
                ]
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    if ($model->status == 1) {
                        return 'Действующий';
                    } elseif ($model->status == 2) {
                        return 'Будущий';
                    } else {
                        return 'Закрытый';
                    }
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Действующий',
                    '2' => 'Будущий',
                    '3' => 'Закрытый',
                ]
            ],
            [
                'attribute' => 'overdue',
                'label' => 'Прос.',
                'value' => function ($model) {
                    if ($model->overdue == 1) {
                        return 'Да';
                    } else {
                        return 'Нет';
                    }
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Просроченный',
                    '0' => 'Не просроченный',

                ]
            ],
            [
                'attribute' => 'contract_pdf',
                'value' => function ($model) {
                    $pdf = !empty($model->contract_pdf) ? '<a href="/uploads/pdf/' . $model->contract_pdf . '" download>'.$model->contract_pdf .'.pdf</a>' : '';
                    return '<a href="/uploads/pdf/' . $model->id . '-' . $model->contract_id . '.pdf" download>'.$model->id . '-' . $model->contract_id.'.pdf</a><br>' . $pdf;
                },
                'format' => 'raw'
            ],
            //'created_at',
            //'updated_at:date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{back} {view} {update} {delete}',
                'buttons' => [
                    'back' => function ($model, $key, $index) {
                        if ($key->status == 3) {
                            return '';
                        } else {
                            return '<a href="' . Url::to(['/admin/contracts/back', 'id' => $index]) . '" title="Возврат депозита" aria-label="Возврат депозита" data-pjax="0" data-confirm="Вы уверены, что хотите сделать возврать депозита?" data-method="post"><span class="glyphicon glyphicon-new-window"></span></a>';
                        }
                    },
                    'delete' => function ($url, $model, $key) {
                        if (Yii::$app->user->can('admin')):
                            return '<a href="' . Url::to(['/admin/products/delete', 'id' => $key]) . '"  title="Удалить" aria-label="Удалить" data-pjax="0"
                            data-method="post"
                            data-confirm="Вы действительно хотите удалить сотрудника?" >
                            <span class="glyphicon glyphicon-trash"></span>
                            </a>';
                        endif;
                    },
                ]
            ],
        ],
    ]); ?>
</div>
