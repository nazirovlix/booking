<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tarifs */

$this->title = 'Редактировать: ' . $model->name;
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="tarifs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
