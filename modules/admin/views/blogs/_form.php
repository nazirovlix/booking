<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;


/* @var $this yii\web\View */
/* @var $model app\models\Blogs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blogs-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'photo')->widget(
                Upload::className(),
                [
                    'url' => ['/admin/file-storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

                ])->label('Главное фото');
            ?>
        </div>
        <div class="col-9">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [ 'height' => 400 ]),
    ]); ?>


    <div class="row">
        <div class="col"> <?= $form->field($model, 'meta_key')->textInput(['maxlength' => true]) ?></div>
        <div class="col"> <?= $form->field($model, 'meta_descriptions')->textarea(['rows' => 6]) ?></div>
    </div>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
