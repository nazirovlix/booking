<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BlogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Blogs'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'Фото',
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="/uploads/' . $model->image . '" style="width:100px">';
                }
            ],
            'title',
            'description:ntext',
            //'content:ntext',
            //'image',
            //'status',
            //'meta_key',
            //'meta_descriptions:ntext',
            //'created_at:date',
            'updated_at:date',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    return $model->status == 1 ? '<p style="color:#0b58a2">Опубликовно</p>' : '<p style="color:#f00">Не опубликовно</p>';
                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Опубликовно',
                    '0' => 'Не опубликовно'
                ]
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
