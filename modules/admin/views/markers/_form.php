<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Markers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="markers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lat')->textInput(['type' => 'hidden'])->label(false) ?>

    <?= $form->field($model, 'lng')->textInput(['type' => 'hidden'])->label(false) ?>

    <?php if(!empty($model->lat)):?>
        <button type="button" class="btn btn-success" onclick="openMap()">Изменить Адрес точки на Google карте</button>
        <hr>
    <?php endif;?>
    <div id="google-map" class="ordering-map <?= !empty($model->lat) ? 'hidden' : '' ?>" style="margin-bottom: 30px">

        <div class="ordering-map-h4" style="margin-bottom: 10px">
            <h4>
               Адрес точки (щелкните мышкой по нужному здания и отобразится точка)
            </h4>
        </div>
        <div class="ordering-map-img">

            <div id="floating-panel">
                <input class="btn btn-danger" onclick="deleteMarkers();" type=button value="Удалить точку">
            </div>
            <div class="field" id="map" style="height: 300px; width: 100%;">
                <script>
                    var map;
                    var markers = [];
                    var i = 1;
                    var lat;
                    var lng;

                    function initMap() {
                        var haightAshbury = {lat: 55.756576, lng: 37.623676};

                        map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 14,
                            center: haightAshbury,
                            mapTypeId: 'terrain'
                        });


                        // This event listener will call addMarker() when the map is clicked.
                        map.addListener('click', function (event) {
                            deleteMarkers();
                            if (i == 1) {
                                addMarker(event.latLng);
                                i = 2;
                            }
                        });
                    }

                    function openMap(){
                        $('#google-map').removeClass('hidden');
                    }

                    // Adds a marker to the map and push to the array.
                    function addMarker(location) {

                        var marker = new google.maps.Marker({
                            position: location,
                            map: map
                        });
                        markers.push(marker);

                        $('#markers-lat').val(marker.getPosition().lat());
                        $('#markers-lng').val(marker.getPosition().lng());

                    }


                    // Sets the map on all markers in the array.
                    function setMapOnAll(map) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);
                        }

                    }

                    // Removes the markers from the map, but keeps them in the array.
                    function clearMarkers() {
                        setMapOnAll(null);
                    }

                    // Shows any markers currently in the array.
                    function showMarkers() {
                        setMapOnAll(map);
                    }

                    // Deletes all markers in the array by removing references to them.
                    function deleteMarkers() {
                        clearMarkers();
                        i = 1;
                        $('#markers-lat').val('');
                        $('#markers-lng').val('');
                        markers = [];
                    }


                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                </script>

            </div>

        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
