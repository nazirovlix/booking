<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Markers */

$this->title = 'Адрес точки: ' . $model->city . ", " . $model->address;
if (Yii::$app->user->can('admin')):
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Markers'), 'url' => ['index']];
endif;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="markers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('admin')): ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'При удалении Адресной точки удаляеться все продукты и сотрудники в этой точке. Вы уверены? '),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'city',
            'address',
            'lat',
            'lng',
        ],
    ]) ?>
    <input id="lat" class="hidden" value="<?= $model->lat; ?>"/>
    <input id="lng" class="hidden" value="<?= $model->lng; ?>"/>

    <h4>Точка на карте</h4>
    <div class="field" id="map2" style="height: 200px; width: 100%; margin-top: 30px">
        <script>


            function initMap() {
                var lat1 = Number(document.getElementById("lat").value);
                var lng1 = Number(document.getElementById("lng").value);
                var uluru = {lat: lat1, lng: lng1};
                var map = new google.maps.Map(document.getElementById('map2'), {
                    zoom: 15,
                    center: uluru

                });
                var marker = new google.maps.Marker({

                    position: uluru,
                    map: map
                });
            }


        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
        </script>

    </div>

</div>
<hr>
<div class="markers-view" style="margin-top: 30px">
    <h1>Сотрудники на точке</h1>
    <?= GridView::widget([
        'dataProvider' => $workers,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            'fio',
            'passport_n',
            'phone',
            'bday',
            //'snils',
            'position.name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return '<a href="' . Url::to(['worker/view', 'id' => $key]) . '" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
                    },
                    'update' => function ($url, $model, $key) {
                        return '<a href="' . Url::to(['worker/update', 'id' => $key]) . '" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>';
                    },
                ]
            ],
        ],
    ]); ?>
</div>
<hr>
<div class="markers-view" style="margin-top: 30px">
    <h1>Устройствa на точке</h1>
    <?= GridView::widget([
        'dataProvider' => $products,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'serial_number',
            'extension_number',
            //'image',
            //'description:ntext',
            //'content:ntext',
            //'free',
            //'status',
            //'marker_id',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return '<a href="' . Url::to(['products/view', 'id' => $key]) . '" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>';
                    },
                    'update' => function ($url, $model, $key) {
                        return '<a href="' . Url::to(['products/update', 'id' => $key]) . '" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>';
                    },
                ]
            ],
        ],
    ]); ?>
</div>