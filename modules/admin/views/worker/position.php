<?php

use app\models\Markers;
use app\models\Positions;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Create Worker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-create">

    <div class="nav-tabs-custom">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link" href="<?= Url::to(['/admin/worker/create', 'id' => $user_id ?: '']) ?>">Регистрация</a>
                <a class="nav-item nav-link"
                   href="<?= Url::to(['/admin/worker/info', 'user_id' => $user_id, 'id' => true]) ?>">Дополнительная информация</a>
                <a class="nav-item nav-link active show create-worker" href="#">Должность</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane active" style="margin-top: 20px">

                <form action="<?= Url::to(['/admin/worker/position']) ?>" method="get">
                    <label class="control-label">Выберите должность сотрудника</label>
                    <select name="position" class="form-control">
                        <?php if (!empty($positions)): ?>
                            <?php foreach ($positions as $position): ?>
                                <option value="<?= $position->id ?>"><?= $position->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <input value="<?= $user_id ?>" type="hidden" name="user_id">

                    <div class="form-group" style="float: right; margin: 20px 0">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                </form>


            </div>
        </div>
        <div class="grid-view">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Логин</th>
                    <th>ФИО</th>
                    <th>Доложность</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($workers)): ?>
                    <?php foreach ($workers as $k => $worker): ?>
                        <?php if ($worker->user_id != $user_id): ?>
                            <tr>
                                <td><?= $k + 1 ?></td>
                                <td><?= $worker->user->username ?></td>
                                <td><?= $worker->fio ?></td>
                                <td><?= !empty($worker->position_id) ? $worker->position->name : 'Не задано' ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

    </div>


</div>
