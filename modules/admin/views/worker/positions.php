<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 17.12.2018
 * Time: 2:42
 */
?>
<div class="form-group field-worker-position_id has-success">
    <label class="control-label" for="worker-position_id">Должность</label>
    <select id="worker-position_id" class="form-control" name="Worker[position_id]" aria-invalid="false">
        <option disabled selected>Выберите должность</option>
        <?php if (!empty($positions)): ?>
            <?php foreach ($positions as $item): ?>
                <option value="<?= $item->id ?>"><?= $item->name ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>

    <div class="help-block"></div>
</div>
