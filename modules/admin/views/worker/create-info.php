<?php

use app\models\Markers;
use app\models\Positions;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Create Worker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-create">

    <div class="nav-tabs-custom">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link" href="<?= Url::to(['/admin/worker/create', 'id' => $user->id ?: '']) ?>">Регистрация</a>
                <a class="nav-item nav-link active show create-worker">Дополнительная информация</a>
                <a class="nav-item nav-link" href="#" style="cursor: no-drop">Должность</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane active" style="margin-top: 10px">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['/admin/worker/info','user_id' => $user->id ])
                ]); ?>

                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'passport_n')->textInput(['maxlength' => true])->label('Паспорт №') ?></div>
                    <div class="col-6"><?= $form->field($model, 'phone')->textInput(['type' => 'tel', 'maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'bday')->widget(DatePicker::className(),[
                            'name' => 'dp_2',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,

                            'pluginOptions' => [
                                'defaultViewDate' => '01.01.1990',
                                'autoclose'=> true,
                                'format' => 'dd.mm.yyyy',

                            ]
                        ]) ?>
                    </div>
                    <div class="col-6"><?= $form->field($model, 'snils')->textInput(['maxlength' => true])->label('Снилс') ?></div>
                </div>

                <?= $form->field($model, 'marker_id')->dropDownList(
                    ArrayHelper::map(Markers::find()->all(), 'id', 'address'),
                    ['prompt' => 'Выберите точку']
                ) ?>

                <div class="form-group" style="float: right">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>


</div>
