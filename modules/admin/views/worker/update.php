<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Worker */

$this->title = Yii::t('app', 'Update Worker: ' . $userInfo->id, [
    'nameAttribute' => '' . $userInfo->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $userInfo->id, 'url' => ['view', 'id' => $userInfo->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="worker-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'userInfo' => $userInfo,
        'user' => $user,
        'positions' => $positions
    ]) ?>

</div>
