<?php

use app\models\Markers;
use app\models\Positions;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Worker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worker-form">


    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($user, 'email')->textInput()->label('Почта') ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($user, 'password')->passwordInput()->label('Пароль') ?>
        </div>
        <div class="col-6">
            <?= $form->field($user, 'retypePassword')->passwordInput()->label('Повторите пороль') ?>
        </div>
    </div>
    <hr>

    <?= $form->field($userInfo, 'fio')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-6"><?= $form->field($userInfo, 'passport_n')->textInput(['maxlength' => true])->label('Паспорт №') ?></div>
        <div class="col-6"><?= $form->field($userInfo, 'phone')->textInput(['type' => 'tel', 'maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($userInfo, 'bday')->widget(DatePicker::className(), [
                'name' => 'dp_2',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,

                'pluginOptions' => [
                    'defaultViewDate' => '01.01.1990',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',

                ]
            ]) ?>
        </div>
        <div class="col-6"><?= $form->field($userInfo, 'snils')->textInput(['maxlength' => true])->label('Снилс') ?></div>
    </div>

    <?= $form->field($userInfo, 'marker_id')->dropDownList(
        ArrayHelper::map(Markers::find()->all(), 'id', 'address'),
        [
            'prompt' => 'Выберите точку',
            'class' => 'form-control selected_marker',
            'onChange' => '$("#marker_select").val(this.value)'
        ]
    ) ?>
    <?php if($userInfo->isNewRecord): ?>
    <div id="positions">
        <?= $form->field($userInfo, 'position_id')->dropDownList([],
            [
                'prompt' => 'Выберите должность',
            ]
        ) ?>
    </div>
    <?php else:;?>
        <?= $form->field($userInfo, 'position_id')->dropDownList(ArrayHelper::map($positions,'id','name')) ?>
    <?php endif;?>


    <input type="hidden" id="marker_select">
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
