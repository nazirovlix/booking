<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\ProductPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-page-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'photo')->widget(
        Upload::className(),
        [
            'url' => ['/admin/file-storage/upload'],
            'sortable' => true,
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

        ])->label('Главное фото');
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [ 'height' => 600 ]),
    ]); ?>
    <?= $form->field($model, 'advantages')->widget(MultipleInput::className(), [
        'columns' => [
            [
                'name' => 'adv_icon',
                'type' => 'textInput',
                'options' => [
                    'placeholder' => 'fa-icon'
                ]

            ],
            [
                'name' => 'adv_title',
                'type' => 'textInput',
                'options' => [
                    'placeholder' => 'Название'
                ]

            ],
            [
                'name' => 'adv_content',
                'type' => 'textInput',
                'options' => [
                    'placeholder' => 'Контент'
                ]

            ],

        ],
        'addButtonOptions' => ['class' => 'btn btn-success']
    ])->label('Дополнительные информации') ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
