<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductPage */

$this->title = 'Страница устройств: ' . $model->title;
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="product-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
