<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductPage */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страница устройств', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'attribute' => 'Фото',
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="/uploads/' . $model->image . '" style="width:150px">';
                }
            ],
            'content:ntext',

        ],
    ]) ?>

</div>
