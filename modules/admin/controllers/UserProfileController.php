<?php

namespace app\modules\admin\controllers;

use app\models\search\UserBdays;
use app\models\search\UserProfileBday;
use app\models\User;
use app\models\Worker;
use Yii;
use app\models\UserProfile;
use app\models\search\UserProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserProfileController implements the CRUD actions for UserProfile model.
 */
class UserProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserProfile models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $searchModel = new UserProfileSearch();
        if ($id) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBdays()
    {
        $searchModel = new UserBdays();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!Yii::$app->user->can('admin')) {
            $worker = Worker::findOne(['user_id' => Yii::$app->user->id]);;
            if ($worker)
            {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $worker->marker_id);
            }

        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserProfile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserProfile();
        $user = new User();

        if ($user->load(Yii::$app->getRequest()->post())) {
            $user->username = $user->email;
            $user->retypePassword = $user->password;
            $user->user_type = User::TYPE_CLIENT;
            if ($user->saveModel() && $model->load(Yii::$app->request->post())) {
                $model->user_id = $user->id;
                if (!empty($model->bday)) {
                    $model->bday = strtotime($model->bday);
                }
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    /**
     * Updates an existing UserProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $user = User::findOne(['id' => $model->user_id]);
        if ($user->load(Yii::$app->getRequest()->post()) && $user->saveModel()) {
            if ($model->load(Yii::$app->request->post())) {
                if (!empty($model->bday)) {
                    $model->bday = strtotime($model->bday);
                }
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        if (!empty($model->bday)) {
            $model->bday = date('d.m.Y', $model->bday);
        }
        $user->password = $user->old_password;
        $user->retypePassword = $user->old_password;
        return $this->render('update', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    /**
     * Deletes an existing UserProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $userProfile = UserProfile::findOne($id);
        if ($userProfile) {
            $user = User::findOne($userProfile->user_id);
            if ($user->delete()) {
                return $this->redirect(['index']);
            }
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the UserProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
