<?php

namespace app\modules\admin\controllers;

use app\models\Contracts;
use app\models\Markers;
use app\models\UserProfile;
use app\models\Worker;
use mdm\admin\models\form\Login;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {


        $amountContracts = Contracts::find()->sum('amount');
        $countContracts = Contracts::find()->count();
        $markers = Markers::find()->count();
        $clients = UserProfile::find()->count();
        $contracts = Contracts::find()->orderBy(['id' => SORT_DESC])->limit(10)->all();

        if (!Yii::$app->user->can('admin')) {
            $worker = Worker::findOne(['user_id' => Yii::$app->user->id]);
            if ($worker) {
                $amountContracts = Contracts::find()->where(['marker_id' => $worker->marker_id])->sum('amount');
                $markers = 1;
                $clients = UserProfile::find()->where(['marker_id' => $worker->marker_id])->count();
                $contracts = Contracts::find()->where(['marker_id' => $worker->marker_id])->orderBy(['id' => SORT_DESC])->limit(10)->all();
                $countContracts = Contracts::find()->where(['marker_id' => $worker->marker_id])->count();
            }
        }


        return $this->render('index', compact([
            'amountContracts',
            'countContracts',
            'markers',
            'clients',
            'contracts',
        ]));
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }
}
