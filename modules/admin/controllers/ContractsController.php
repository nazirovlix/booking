<?php

namespace app\modules\admin\controllers;

use app\models\Markers;
use app\models\Products;
use app\models\Tarifs;
use app\models\User;
use app\models\UserProfile;
use app\models\Worker;
use kartik\mpdf\Pdf;
use Mpdf\Mpdf;
use Yii;
use app\models\Contracts;
use app\models\search\ContractsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContractsController implements the CRUD actions for Contracts model.
 */
class ContractsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contracts models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $searchModel = new ContractsSearch();
        if ($id) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contracts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contracts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Contracts();
        $user = new User();
        $userProfile = new UserProfile();
        if (!Yii::$app->user->can('admin')):
            $worker = Worker::findOne(['user_id' => Yii::$app->user->id]);
            if ($worker):
                $markers = Markers::find()->where(['id' => $worker->marker_id])->all();
            endif;
        else:
            $markers = Markers::find()->all();
        endif;
        $last = Contracts::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
        $data = [];

        if ($model->load(Yii::$app->request->post())) {

            //Calculate interval days
            $interval = $model->calculeteAmount($model->date_begin, $model->date_end);
            $model->days = $interval;

            // Calculate Amount
            $sum = $model->calculateInterval($interval);
            $model->amount = !empty($sum) ? $sum : 0;

            $worder = Worker::findOne($model->worker_id);
            $model->marker_id = $worder->marker_id;
            // calculate Contract ID
            if ($last) {
                $contractId = $model->calculateContractId($last, $model);
            } else {
                $contractId = '001';
            }
            $model->contract_id = $contractId . '-' . str_replace('.', '', $model->date_begin);

            if ($model->save()) {

                $contract = $model;
                $this->layout = false;
                $cont = $this->renderPartial('pdf_template', [
                    'id' => $contract->contract_id,
                    'days' => $contract->days,
                    'begin' => $contract->date_begin,
                    'price' => number_format($contract->amount, "0", ' ', ' '),
                    'end' => $contract->date_end,
                    'fio' => $contract->user->fio,
                    'passport' => $contract->user->possport_n,
                    'phone' => $contract->user->phone
                ]);
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($cont);
                // Saves file on the server as 'filename.pdf'
                $mpdf->Output('uploads/pdf/' . $contract->id . '-' . $contract->contract_id . '.pdf', 'F');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'user' => $user,
            'userProfile' => $userProfile,
            'markers' => $markers,
            'dataWorker' => $data,
            'dataProduct' => $data,
            'last' => !empty($last->contract_id) ? $last->contract_id : '0',

        ]);
    }

    /**
     * Updates an existing Contracts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = new User();
        $userProfile = new UserProfile();
        $markers = Markers::find()->all();
        $worker = Worker::findOne($model->worker_id);
        $dataProduct = Products::find()->where(['marker_id' => $worker->marker_id])->all();
        $dataWorker = Worker::find()->where(['marker_id' => $worker->marker_id])->all();
        $last = Contracts::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();

        if ($model->load(Yii::$app->request->post())) {
            //Calculate interval days
            $interval = $model->calculeteAmount($model->date_begin, $model->date_end);
            $model->days = $interval;

            // Calculate Amount
            $sum = $model->calculateInterval($interval);
            $model->amount = !empty($sum) ? $sum : 0;
            $worder = Worker::findOne($model->worker_id);
            $model->marker_id = $worder->marker_id;
            if ($model->save()) {
                $contract = $model;
                $this->layout = false;
                $cont = $this->renderPartial('pdf_template', [
                    'id' => $contract->contract_id,
                    'days' => $contract->days,
                    'begin' => $contract->date_begin,
                    'price' => number_format($contract->amount, "0", ' ', ' '),
                    'end' => $contract->date_end,
                    'fio' => $contract->user->fio,
                    'passport' => $contract->user->possport_n,
                    'phone' => $contract->user->phone
                ]);
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($cont);
                // Saves file on the server as 'filename.pdf'
                $mpdf->Output('uploads/pdf/' . $contract->id . '-' . $contract->contract_id . '.pdf', 'F');

                return $this->redirect(['view', 'id' => $model->id]);
            }


        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user,
            'userProfile' => $userProfile,
            'markers' => $markers,
            'dataWorker' => $dataWorker,
            'dataProduct' => $dataProduct,
            'markerId' => $worker->marker_id,
            'last' => !empty($last->contract_id) ? $last->contract_id : '0'
        ]);
    }

    public function actionExtend($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            //Calculate interval days
            $interval = $model->calculeteAmount($model->date_begin, $model->date_end);
            $model->days = !empty($interval) ? $interval : 0;
            // Calculate Amount
            $sum = $model->calculateInterval($interval);
            $model->amount = !empty($sum) ? $sum : 0;
            $model->contract_pdf = 'P' . $model->id . '-' . $model->contract_id . '.pdf';
            if ($model->save(false)) {
                $contract = $model;
                $this->layout = false;
                $cont = $this->renderPartial('pdf_template', [
                    'id' => $contract->contract_id,
                    'days' => $contract->days,
                    'begin' => $contract->date_begin,
                    'price' => number_format($contract->amount, "0", ' ', ' '),
                    'end' => $contract->date_end,
                    'fio' => $contract->user->fio,
                    'passport' => $contract->user->possport_n,
                    'phone' => $contract->user->phone
                ]);
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($cont);
                // Saves file on the server as 'filename.pdf'
                $mpdf->Output('uploads/pdf/P' . $contract->id . '-' . $contract->contract_id . '.pdf', 'F');

                return $this->redirect(['view', 'id' => $model->id]);
            }

        }
        return $this->render('extend', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Contracts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionBack($id)
    {
        $model = Contracts::findOne($id);
        $model->status = 3;

        if ($model->save()) {
            $searchModel = new ContractsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Finds the Contracts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contracts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contracts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     *  Ajax for User select
     * @param $value
     * @param $users
     * @return string
     */
    public function actionGetUser($value = null)
    {
        if ($value) {
            $users = UserProfile::find()
                ->where(['like', 'fio', $value])
                ->orWhere(['like', 'possport_n', $value])
                ->all();
            return $this->renderAjax('select-user-ajax', [
                'users' => $users
            ]);
        } else {
            return $this->renderAjax('select-user-ajax');
        }

    }

    /**
     * Create User on Model
     * @return string|\yii\web\Response
     */
    public function actionCreateUser()
    {
        $reqUser = Yii::$app->request->get('User');
        $reqUserProfile = Yii::$app->request->get('UserProfile');
        $user = new User();
        $userModel = new UserProfile();

        if ($reqUser && $reqUserProfile) {
            if ($user->saveFromReq($reqUser) && $userModel->saveFromReq($reqUserProfile, $user->id)) {

                return $this->renderAjax('selected-user-ajax', [
                    'user' => $userModel
                ]);
            }
        }
        return 2;
    }

    public function actionGetWorker($id)
    {
        if ($id) {
            $workers = Worker::find()->where(['marker_id' => $id])->all();
            $products = Products::find()
                ->where(['marker_id' => $id])
                ->limit(20)
                ->all();


            return $this->renderAjax('select-workers-ajax', [
                'workers' => $workers,
                'products' => $products,
            ]);
        }
        return false;
    }

    public function actionGetProduct($value = null)
    {
        if ($value) {
            $products = Products::find()
                ->where(['like', 'name', $value])
                ->orWhere(['like', 'barcode', $value])
                ->orWhere(['like', 'serial_number', $value])
                ->all();
            return $this->renderAjax('select-product-ajax', [
                'products' => $products
            ]);
        } else {
            return $this->renderAjax('select-product-ajax');
        }

    }

    public function actionPdf($id)
    {
        $contract = Contracts::findOne($id);
        $this->layout = false;
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $this->renderPartial('pdf_template', [
                'id' => $contract->contract_id,
                'days' => $contract->days,
                'begin' => $contract->date_begin,
                'price' => number_format($contract->amount, "0", ' ', ' '),
                'end' => $contract->date_end,
                'fio' => $contract->user->fio,
                'passport' => $contract->user->possport_n,
                'phone' => $contract->user->phone
            ]),
            'options' => [
                'title' => 'Договор',
                'subject' => 'Договор аренды оборудования'
            ],
            'methods' => [
                //'SetHeader' => ['Договор аренды оборудования'],
            ]
        ]);

        return $pdf->render();
    }

    public function actionPdfView()
    {
        $get = Yii::$app->getRequest()->get();
        debug($get, 1);
    }

    public function actionCal()
    {

        $model = new Contracts();

        $sum = $model->calculateInterval(16);
        echo 'sum:' . $sum;
        die;
    }

}
