<?php

namespace app\modules\admin\controllers;

use app\models\Positions;
use app\models\User;
use mdm\admin\models\form\Signup;
use Yii;
use app\models\Worker;
use app\models\search\WorkerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WorkerController implements the CRUD actions for Worker model.
 */
class WorkerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Worker models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $searchModel = new WorkerSearch();
        if ($id) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Worker model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $user = new User();
        $userInfo = new Worker();
        if ($user->load(Yii::$app->getRequest()->post()) && $userInfo->load(Yii::$app->getRequest()->post())) {
            $user->username = $user->email;

            if ($user->saveModel()) {
                $userInfo->user_id = $user->id;
                $userInfo->save();
                return $this->redirect(['view', 'id' => $userInfo->id]);
            }
        }
        return $this->render('create', [
            'user' => $user,
            'userInfo' => $userInfo,
        ]);
    }

    public function actionUserPosition($id = null)
    {
        $positions = new Positions();
        $positions = $positions->getPositions($id);
        return $this->renderAjax('positions',['positions' => $positions]);

    }


    /**
     * Updates an existing Worker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $userInfo = $this->findModel($id);
        $user = User::findOne($userInfo->user_id);
        $user->password = $user->old_password;
        $user->retypePassword = $user->old_password;

        $positions = new Positions;
        $positions = $positions->getPositions($userInfo->marker_id);
        $position = Positions::findOne($userInfo->position_id);
        $positions[] = $position;



        if ($user->load(Yii::$app->getRequest()->post()) && $userInfo->load(Yii::$app->getRequest()->post())) {
            $user->username = $user->email;
            if ($user->saveModel() && $userInfo->save()) {
                return $this->redirect(['view', 'id' => $userInfo->id]);
            }
        }
        return $this->render('update', [
            'userInfo' => $userInfo,
            'user' => $user,
            'positions' => $positions

        ]);
    }

    /**
     * Deletes an existing Worker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (($model = Worker::findOne($id)) !== null) {
            $user = User::findOne($model->user_id);
            if ($user->delete()) {
                return $this->redirect(['index']);
            }
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the Worker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Worker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Worker::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
