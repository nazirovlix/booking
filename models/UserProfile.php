<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $fio
 * @property string $possport_n
 * @property string $phone
 * @property string $bday
 * @property int $blocked
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'fio', 'phone'], 'required'],
            [['user_id', 'blocked'], 'integer'],
            [['fio', 'possport_n', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['marker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Markers::className(), 'targetAttribute' => ['marker_id' => 'id']],
            [['bday'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'fio' => "ФИО",
            'marker_id' => "Адрес точки",
            'possport_n' => "Паспорт №",
            'phone' => "Номер телефона",
            'bday' => "Дата рождения",
            'blocked' => "Заблокировать",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function saveFromReq($reqUserProfile, $user_id){

        $this->user_id      = $user_id;
        $this->fio          = $reqUserProfile['fio'];
        $this->possport_n   = $reqUserProfile['possport_n'];
        $this->phone        = $reqUserProfile['phone'];
        $this->bday         = strtotime($reqUserProfile['bday']);
        $this->blocked      = $reqUserProfile['blocked'];

        if($this->save()){
            return true;
        }

        return false;

    }
}
