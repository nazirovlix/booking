<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contracts;

/**
 * ContractsSearch represents the model behind the search form of `app\models\Contracts`.
 */
class ContractsSearch extends Contracts
{

    public $product_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'worker_id', 'product_id', 'user_id', 'user_id', 'amount', 'payed_status', 'status', 'created_at', 'updated_at', 'overdue'], 'integer'],
            [['date_begin', 'date_end', 'contract_pdf', 'contract_id','product_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        $query = Contracts::find()->orderBy(['id' => SORT_DESC]);

        if ($id) {
            $query->where(['marker_id' => $id]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contracts.worker_id' => $this->worker_id,
            'contracts.user_id' => $this->user_id,
//            'product_id' => $this->product_id,
            'contracts.amount' => $this->amount,
            'contracts.payed_status' => $this->payed_status,
            'contracts.status' => $this->status,
            'contracts.overdue' => $this->overdue,
            'contracts.created_at' => $this->created_at,
            'contracts.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'contracts.date_begin', $this->date_begin])
            ->andFilterWhere(['like', 'contracts.contract_id', $this->contract_id])
            ->andFilterWhere(['like', 'contracts.date_end', $this->date_end])
            ->andFilterWhere(['like', 'contracts.contract_pdf', $this->contract_pdf])
            ->joinWith('product')
            ->andFilterWhere(['like', 'products.serial_number', $this->product_name])
            ->andFilterWhere(['like', 'products.name', $this->product_name ]);

        return $dataProvider;
    }
}
