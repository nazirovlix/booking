<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserProfile;

/**
 * UserProfileSearch represents the model behind the search form of `app\models\UserProfile`.
 */
class UserBdays extends UserProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'blocked'], 'integer'],
            [['fio', 'possport_n', 'phone', 'bday'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id = null)
    {
        $query = UserProfile::find()
        ->where('month(FROM_UNIXTIME(bday)) = month(current_date)
and dayofmonth(FROM_UNIXTIME(bday)) > dayofmonth(current_date)
or month(FROM_UNIXTIME(bday)) = if(month(current_date)+1 = 13, 1, month(current_date)+1)
and dayofmonth(FROM_UNIXTIME(bday)) < dayofmonth(current_date)');
        //->orderBy(['bdaa' => SORT_ASC]);

        if($id){
           $query->where(['marker_id' => $id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'blocked' => $this->blocked,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'possport_n', $this->possport_n])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'bday', $this->bday]);

        return $dataProvider;
    }
}
