<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Advantages;

/**
 * AdvantagesSearch represents the model behind the search form of `app\models\Advantages`.
 */
class AdvantagesSearch extends Advantages
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'content', 'icon1', 'title1', 'content1', 'icon2', 'title2', 'content2', 'icon3', 'title3', 'content3'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Advantages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'icon1', $this->icon1])
            ->andFilterWhere(['like', 'title1', $this->title1])
            ->andFilterWhere(['like', 'content1', $this->content1])
            ->andFilterWhere(['like', 'icon2', $this->icon2])
            ->andFilterWhere(['like', 'title2', $this->title2])
            ->andFilterWhere(['like', 'content2', $this->content2])
            ->andFilterWhere(['like', 'icon3', $this->icon3])
            ->andFilterWhere(['like', 'title3', $this->title3])
            ->andFilterWhere(['like', 'content3', $this->content3]);

        return $dataProvider;
    }
}
