<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "positions".
 *
 * @property int $id
 * @property string $name
 *
 * @property Worker[] $workers
 * @property Positions[] $positions
 *
 */
class Positions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'positions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','count'], 'required'],
            [['count'],'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Должность'),
            'count' => Yii::t('app', 'Количество возможных в одном точке'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['position_id' => 'id']);
    }

    /**
     * Get position | calculate from count
     * @return array
     *
     */
    public function getPositions($marker_id){

        $workers = Worker::find()
            ->where(['marker_id' => $marker_id])
            ->all();
        $allPositions = Positions::find()->all();
        $positions = [];
        foreach ($allPositions as $position){
            $k = 0;
            foreach ($workers as $item){
                if(!empty($item->position_id) && ($item->position_id == $position->id)){
                    $k++;
                }
            }
            if($position->count > $k){
                $positions[] = $position;
            }
        }
        return $positions;
    }
}
