<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tips".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $image
 * @property int $status
 * @property string $meta_key
 * @property string $meta_descriptions
 * @property int $created_at
 * @property int $updated_at
 */
class Tips extends \yii\db\ActiveRecord
{
    public $photo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tips';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'photo',
                'pathAttribute' => 'image',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'content', 'meta_descriptions'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'image', 'meta_key'], 'string', 'max' => 255],
            [['photo'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => "Название",
            'photo' => 'Фото',
            'description' => "Описание",
            'content' => "Контент",
            'image' => "Фото",
            'status' => "Статус",
            'meta_key' => Yii::t('app', 'Meta Key'),
            'meta_descriptions' => Yii::t('app', 'Meta Descriptions'),
            'created_at' => "Создан",
            'updated_at' => "Обнавлен",
        ];
    }
}
