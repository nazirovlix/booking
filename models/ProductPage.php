<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "product_page".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $image
 *
 * @property ProductAdvantages[] $productAdvantages
 */
class ProductPage extends \yii\db\ActiveRecord
{
    public $photo;
    public $advantages;
    public $adv_icon;
    public $adv_title;
    public $adv_content;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_page';
    }

    public function behaviors()
    {
        return [

            [
                'class' => UploadBehavior::className(),
                'attribute' => 'photo',
                'pathAttribute' => 'image',
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [['photo','advantages','adv_icon','adv_title','adv_content'], 'safe']
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(is_array($this->advantages)){
            if(!$this->isNewRecord){
                ProductAdvantages::deleteAll(['product_id' => $this->id]);
            }

            foreach ($this->advantages as $item){
                try{
                    Yii::$app->db->createCommand()->insert('product_advantages',[
                        'product_id' => $this->id,
                        'icon' => $item['adv_icon'],
                        'title' => $item['adv_title'],
                        'content' => $item['adv_content']
                    ])->execute();
                } catch (Exception $e){
                    echo $e->getMessage();
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    // Set Product Advantages
    public function afterFind()
    {
        $advantages = ProductAdvantages::find()->where(['product_id' => $this->id])->all();
        $j = 0;
        foreach ($advantages as $item):
            $this->advantages[$j]['adv_icon'] = $item->icon;
            $this->advantages[$j]['adv_title'] = $item->title;
            $this->advantages[$j]['adv_content'] = $item->content;
            $j++;
        endforeach;
        parent::afterFind();
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'content' => Yii::t('app', 'Контент'),
            'image' => Yii::t('app', 'Фото'),
            'photo' => Yii::t('app', 'Фото'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAdvantages()
    {
        return $this->hasMany(ProductAdvantages::className(), ['product_id' => 'id']);
    }
}
