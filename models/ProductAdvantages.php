<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_advantages".
 *
 * @property int $id
 * @property int $product_id
 * @property string $icon
 * @property string $title
 * @property string $content
 *
 * @property Products $product
 */
class ProductAdvantages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_advantages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id'], 'integer'],
            [['content'], 'string'],
            [['icon', 'title'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Page'),
            'icon' => "Иконка",
            'title' => "Название",
            'content' => "Контент",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ProductPage::className(), ['id' => 'product_id']);
    }
}
