<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarifs".
 *
 * @property int $id
 * @property string $name
 * @property int $day_count
 * @property int $price
 */
class Tarifs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarifs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day_count', 'price'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'day_count' => Yii::t('app', 'Количество дней'),
            'price' => Yii::t('app', 'Цена'),
        ];
    }
}
