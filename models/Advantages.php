<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "advantages".
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property string $icon1
 * @property string $title1
 * @property string $content1
 * @property string $icon2
 * @property string $title2
 * @property string $content2
 * @property string $icon3
 * @property string $title3
 * @property string $content3
 */
class Advantages extends \yii\db\ActiveRecord
{

    public $image1;
    public $image2;
    public $image3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advantages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image1',
                'pathAttribute' => 'icon1',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image2',
                'pathAttribute' => 'icon2',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image3',
                'pathAttribute' => 'icon3',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['content', 'content1', 'content2', 'content3'], 'string'],
            [['name', 'icon1', 'title1', 'icon2', 'title2', 'icon3', 'title3'], 'string', 'max' => 255],
            [['image1', 'image2', 'image3'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => "Название",
            'content' => "Контент",
            'image1' => 'Фото-1',
            'image2' => 'Фото-2',
            'image3' => 'Фото-3',
            'icon1' => "1-Икона",
            'title1' => "1-Название",
            'content1' => "1-Контент",
            'icon2' => "2-Икона",
            'title2' => "2-Название",
            'content2' => "3-Контент",
            'icon3' => "3-Икона",
            'title3' => "3-Название",
            'content3' => "3-Контент",
        ];
    }
}
