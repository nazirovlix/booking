<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "worker".
 *
 * @property int $id
 * @property int $user_id
 * @property string $fio
 * @property string $passport_n
 * @property string $phone
 * @property string $bday
 * @property string $snils
 * @property int $position_id
 * @property int $marker_id
 *
 * @property Markers $marker
 * @property Positions $position
 * @property User $user
 */
class Worker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'worker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
       // $class = Yii::$app->getUser()->identityClass ? : 'mdm\admin\models\User';
        return [

            [['user_id', 'fio', 'passport_n', 'marker_id'], 'required'],
            ['user_id', 'unique'],
            [['user_id', 'position_id', 'marker_id'], 'integer'],
            [['fio', 'passport_n', 'phone', 'bday', 'snils'], 'string', 'max' => 255],
            [['marker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Markers::className(), 'targetAttribute' => ['marker_id' => 'id']],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Positions::className(), 'targetAttribute' => ['position_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'user.username' => Yii::t('app', 'Логин'),
            'fio' => Yii::t('app', 'ФИО'),
            'passport_n' => 'Паспорт №',
            'phone' => Yii::t('app', 'Телефон'),
            'bday' => Yii::t('app', 'День Рождения'),
            'snils' => Yii::t('app', 'Снилс'),
            'position_id' => Yii::t('app', 'Должность'),
            'position.name' => Yii::t('app', 'Должность'),
            'marker_id' => Yii::t('app', 'Точка ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarker()
    {
        return $this->hasOne(Markers::className(), ['id' => 'marker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Positions::className(), ['id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
