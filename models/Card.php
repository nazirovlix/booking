<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "card".
 *
 * @property int $id
 * @property string $card_number
 * @property string $tariff
 * @property string $operator
 * @property int $extension
 */
class Card extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['card_number'], 'required'],
            [['extension'], 'integer'],
            [['card_number', 'tariff', 'operator'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'card_number' => "Номер карты связи",
            'tariff' => "Тариф",
            'operator' => "Оператор",
            'extension' => "Необходимо ли посуточное продление?",
        ];
    }
}
