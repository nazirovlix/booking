<?php

namespace app\models;

use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contracts".
 *
 * @property int $id
 * @property int $worker_id
 * @property int $user_id
 * @property int $product_id
 * @property string $date_begin
 * @property string $date_end
 * @property int $amount
 * @property string $contract_pdf
 * @property string $card_number
 * @property int $payed_status
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $contract_id
 * @property int $marker_id
 * @property int $days
 * @property int $count
 * @property int $one_day
 * @property int $address_id
 *
 * @property Products $product
 * @property User $user
 * @property User $worker
 */
class Contracts extends \yii\db\ActiveRecord
{

    public $product_name;
    public $one_day;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contracts';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['worker_id', 'user_id', 'product_id', 'date_begin', 'date_end'], 'required'],
            [['worker_id', 'user_id', 'amount', 'product_id', 'payed_status', 'status', 'created_at', 'updated_at', 'overdue', 'days'], 'integer'],
            [['date_begin', 'date_end', 'contract_pdf', 'card_number', 'contract_id'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['worker_id' => 'id']],
            [['marker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Markers::className(), 'targetAttribute' => ['marker_id' => 'id']],
            [['product_name','count','one_day','address_id'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contract_id' => "Номер договора",
            'worker_id' => "Сотрудник",
            'user_id' => "Клиент",
            'marker_id' => "Адрес точки",
            'product_id' => "Устройство",
            'address_id' => "Адрес доставки",
            'product_name' => "Уст-о или номер",
            'date_begin' => "Дата начала",
            'date_end' => "Дата окончания",
            'card_number' => "Номер карты связи",
            'amount' => "Сумма договора",
            'contract_pdf' => "Договор PDF",
            'payed_status' => "Тип оплаты",
            'status' => "Статус договора",
            'overdue' => "Просроченный",
            'count' => "Количество",
            'created_at' => "Создан",
            'updated_at' => "Обновлен",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }

    /**
     * Return interval days
     * @param null $day
     * @return bool|float
     */
    public function calculeteAmount($begin = null, $day = null)
    {
        if ($day && $begin) {
            $datetime1 = strtotime($begin);
            $datetime2 = strtotime($day);
            $interval = ($datetime2 - $datetime1) / (60 * 60 * 24);
            return floor($interval);

        }
        return false;
    }

    public function calculateSum($interval, $sum = 0)
    {
        $id = 0;
        $tariffs = Tarifs::find()->all();
        foreach ($tariffs as $item) {
            if ($interval >= $item->day_count)
                $id = $item->id;
        }

        if ($id) {
            $tarif = Tarifs::find()->where(['id' => $id])->one();
            $sum += $tarif['price'];
            $int = $interval - $tarif['day_count'];
            if ($int > 0) {
                return $this->calculateSum($int, $sum);
            }
        }
        return $sum;


    }

    public function calculateInterval($interval)
    {
        $tariff = Tarifs::find()->where(['day_count' => $interval])->one();
        if ($tariff) {
            return $tariff->price;
        } else {
            return $this->calculateSum($interval);
        }

    }

    public function calculateContractId($last, $model)
    {
        $last_day = substr($last->contract_id, 4, 2);
        $last_month = substr($last->contract_id, 6, 2);
        $last_count = (int)substr($last->contract_id, 0, 3);
        $begin_day = substr($model->date_begin, 0, 2);
        $begin_month = substr($model->date_begin, 3, 2);

        if ($last_day == $begin_day && $last_month == $begin_month) {
            $contractId = '00' . ($last_count + 1);
        } else {
            $contractId = '001';
        }
        return $contractId;
    }

    public function saveToSession($post)
    {
        $model = new Contracts();
        $product = Products::find()->limit(1)->one();
        $last = Contracts::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
        if ($model->load($post)) {
            //Calculate interval days
            $interval = $model->calculeteAmount($model->date_begin, $model->date_end);
            $model->days = $interval;

            // Calculate Amount
            $sum = $model->calculateInterval($interval);
            $model->amount = !empty($sum) ? $sum : 0;


            $model->product_id = $product->id;
            if ($last) {
                $contractId = $model->calculateContractId($last, $model);
            } else {
                $contractId = '001';
            }
            $model->contract_id = $contractId . '-' . str_replace('.', '', $model->date_begin);
            $model->amount = $model->amount * $model->count;
            $model->one_day = $model->amount / $model->days;
        }
        return $model;
    }

    public function saveFromSession()
    {
        $session = Yii::$app->session['contract'];
        $worker = Worker::findOne(['marker_id' => $session['marker_id']]);
        $userProfovile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);
        $contract = new Contracts();
        $contract->marker_id = $session['marker_id'];
        $contract->worker_id = !empty($worker->id) ? (int)$worker->id : (int) Worker::find()->select('id')->one()->id;
        $contract->contract_id = $session['contract_id'];
        $contract->user_id = $userProfovile->id;
        $contract->product_id = $session['product_id'];
        $contract->date_begin = $session['date_begin'];
        $contract->payed_status = $session['payed_status'];
        $contract->status = 2;
        $contract->date_end = $session['date_end'];
        $contract->amount = $session['amount'];
        $contract->days = $session['days'];
        $contract->count = $session['count'];
        if(!empty($session['address_id'])){
            $contract->address_id = $session['address_id'];
        }
        if($contract->save(false)){

            return $contract;
        }
        return false;

    }


}
