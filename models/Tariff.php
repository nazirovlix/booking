<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff".
 *
 * @property int $id
 * @property string $name
 * @property string $price
 * @property string $speed
 * @property string $app
 * @property string $battery
 * @property string $extra
 * @property string $calls
 * @property string $hotspot
 * @property int $status
 */
class Tariff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['status'], 'integer'],
            [['name', 'price', 'speed', 'app', 'battery', 'extra', 'calls', 'hotspot'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'price' => Yii::t('app', 'Цена'),
            'speed' => Yii::t('app', 'Скорость'),
            'app' => Yii::t('app', 'App'),
            'battery' => Yii::t('app', 'Срок службы батареи'),
            'extra' => Yii::t('app', 'Допольнительно '),
            'calls' => Yii::t('app', 'Voip звонки'),
            'hotspot' => Yii::t('app', 'Горячая точка'),
            'status' => Yii::t('app', 'Статус Оптимальный'),
        ];
    }
}
