<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $serial_number
 * @property string $extension_number
 * @property string $image
 * @property string $description
 * @property string $content
 * @property int $free
 * @property int $amount
 * @property int $barcode
 * @property int $status
 * @property int $marker_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Contracts[] $contracts
 * @property ProductAdvantages[] $productAdvantages
 * @property ProductImages[] $productImages
 * @property Markers $marker
 */
class Products extends \yii\db\ActiveRecord
{

    public $photo;
    public $moreImages;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'photo',
                'pathAttribute' => 'image',
            ],
//            [
//                'class' => UploadBehavior::className(),
//                'attribute' => 'moreImages',
//                'multiple' => true,
//                'uploadRelation' => 'productImages',
//                'pathAttribute' => 'images',
//                'baseUrlAttribute' => false,
//                'orderAttribute' => 'sort',
//            ],
        ];
    }

    public function afterFind()
    {
        $contract = Contracts::find()->where(['product_id' => $this->id])->one();
        if(!empty($contract)){
            $this->status = '0';
        }
        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'serial_number', 'extension_number', 'marker_id'], 'required'],
            [['description', 'content','barcode'], 'string'],
            [['free', 'status', 'marker_id', 'created_at', 'updated_at','amount'], 'integer'],
            [['name', 'serial_number', 'extension_number', 'image'], 'string', 'max' => 255],
            [['marker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Markers::className(), 'targetAttribute' => ['marker_id' => 'id']],
            [['photo', 'moreImages'], 'safe']
        ];
    }

    /**
     * Save Product Advantages
     * {@inheritdoc}
     */

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => "Название",
            'barcode' => "Штрих код",
            'serial_number' => "Серийный номер",
            'extension_number' => "Внутренний номер",
            'image' => "Фото",
            'description' => "Описание",
            'content' => "Контент",
            'free' => "Занято ли устройство",
            'amount' => "Цена за 1 день",
            'status' => "Статус",
            'marker_id' => "Точка продаж",
            'created_at' => "Создан",
            'updated_at' => "Обновлен",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contracts::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarker()
    {
        return $this->hasOne(Markers::className(), ['id' => 'marker_id']);
    }
    public function getProductInfo()
    {
        if($this->status == 0)
        {
            $status = 'Списано';
        } elseif($this->status == 1)
        {
            $status = 'Рабочее';
        } else
        {
            $status = 'В ремонте';
        }
        return $this->name . ' ( '.$this->serial_number. ' , '. $status .')';
    }
}
