<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $old_password
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_type
 * @property integer $retypePassword
 * @property string $authKey
 * @property string $password write-only password
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;
    const TYPE_WORKER = 1;
    const TYPE_CLIENT = 2;

    public $password;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return "{{%user}}";
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $class = Yii::$app->getUser()->identityClass ? : 'mdm\admin\models\User';
        return [
            [['username', 'password', 'email'], 'required'],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],

            [['username'], 'string','min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => $class, 'message' => 'Этот логин уже занят'],
            ['email', 'unique', 'targetClass' => $class, 'message' => 'Этот email уже занят'],
            [['email'], 'email'],
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['password', 'retypePassword','old_password'], 'string', 'min' => 6],
            ['retypePassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
          'email' => 'Email',
          'password' => 'Пароль',
          'retypePassword' => 'Повторный пароль',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function saveModel()
    {
        $save_status = false;
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        $this->auth_key = Yii::$app->security->generateRandomString();
        $this->old_password = $this->password;
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        $this->user_type = !empty($this->user_type) ? $this->user_type : self::TYPE_WORKER;
        $save_status = $this->save();
        if ($save_status) {
            $transaction->commit();
            \Yii::$app->mail->compose()
                ->setFrom('neofarmuzb@gmail.com')
                ->setTo('nazirovlix@gmail.com')
                ->setSubject('Авторизация')
                ->setTextBody('Пожалуйста пройдит по ссылке ' . Url::to(['http://booking.loc/site/auth','key' => $this->auth_key]))
                ->send();
            return true;
        } else {
            $transaction->rollBack();
        }

        return false;
    }

    /**
     * @param $reqUser
     * @return bool
     */

    public function saveFromReq($reqUser)
    {
        $this->username = $reqUser['username'];
        $this->password = $reqUser['password'];
        $this->email    = $reqUser['email'];
        $this->user_type = User::TYPE_CLIENT;
        if($this->saveModel()){
            return true;
        }
        return false;
    }

    public function saveGuest()
    {

        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        $this->auth_key = Yii::$app->security->generateRandomString();
        $this->username = $this->email;
        $this->old_password = $this->password;
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        $this->user_type = self::TYPE_CLIENT;
        $this->status = self::STATUS_ACTIVE;

        if ($this->save(false)) {
            return true;
        }
        return false;
    }

}
