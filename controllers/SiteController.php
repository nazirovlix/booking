<?php

namespace app\controllers;

use app\models\About;
use app\models\Address;
use app\models\Advantages;
use app\models\Contracts;
use app\models\FaqCategory;
use app\models\forms\Login;
use app\models\forms\Signup;
use app\models\Markers;
use app\models\ProductPage;
use app\models\Products;
use app\models\Socials;
use app\models\Tariff;
use app\models\User;
use app\models\UserProfile;
use YandexCheckout\Client;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $markers = Markers::find()->all();
        $model = new Contracts();
        $advantages = Advantages::findOne(1);

        return $this->render('index', [
            'markers' => $markers,
            'model' => $model,
            'advantages' => $advantages,
        ]);
    }

    /**
     * @return string
     */
    public function actionOrder()
    {
        Yii::$app->session->open();
        $session = Yii::$app->session;
        $product = Products::find()->limit(1)->one();

        $model = new Contracts();
        if (Yii::$app->getRequest()->post()) {
            $contract = $model->saveToSession(Yii::$app->getRequest()->post());
            if (!$session['contract'] || (
                    $session['contract'] &&
                    $session['contract']['one_day'] != $contract->one_day &&
                    $session['contract']['amount'] != $contract->amount)) {
                $session['contract'] = [
                    'marker_id' => $contract->marker_id,
                    'contract_id' => $contract->contract_id,
                    'user_id' => !Yii::$app->user->isGuest ? Yii::$app->user->id : '',
                    'product_id' => $contract->product_id,
                    'date_begin' => $contract->date_begin,
                    'date_end' => $contract->date_end,
                    'amount' => $contract->amount,
                    'one_day' => $contract->one_day / $contract->count,
                    'days' => $contract->days,
                    'count' => $contract->count
                ];
            }
        }


        Yii::$app->session->set('url', Url::to(['/site/order']));

        return $this->render('order', [
            'contract' => $session['contract'],
            'product' => $product
        ]);
    }

    public function actionGuest()
    {
        $user = new User();
        $userInfo = new UserProfile();
        $address = new Address();

        if ($user->load(Yii::$app->getRequest()->post()) && $user->saveGuest()) {
            if ($userInfo->load(Yii::$app->getRequest()->post())) {
                $userInfo->user_id = $user->id;
                if ($userInfo->save(false)) {
                    $model = new Login();
                    $model->username = $user->username;
                    $model->password = $user->password;
                    $model->login();
                    $check = Yii::$app->getRequest()->post('radioAddress');
                    if ($check == 4000 || $check == 3000) {
                        Yii::$app->session->setFlash('create-contract');
                        return $this->redirect(Url::to(['/site/contract']));
                    } else {
                        if ($address->load(Yii::$app->getRequest()->post())) {
                            $address->user_id = $user->id;
                            if ($address->save(false)) {
                                return $this->redirect(Url::to(['/site/yandex', 'id' => $address->id]));
                            }
                        }
                    }
                }
            }
        }

        return $this->render('guest', [
            'user' => $user,
            'userInfo' => $userInfo,
            'address' => $address
        ]);
    }

    public function actionOrdering()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->set('url', Url::to(['/site/ordering']));
            return $this->redirect(Url::to(['/site/login']));
        }
        $address = Address::find()->where(['user_id' => Yii::$app->user->id])->all();
        $newAddress = new Address();
        return $this->render('ordering', [
            'address' => $address,
            'newAddress' => $newAddress
        ]);
    }

    public function actionYandex()
    {

        $id = Yii::$app->request->get('id');

        if($id == 4000){
            Yii::$app->session->setFlash('create-contract');
            return $this->redirect(Url::to(['/site/cabinet']));
        }
        $session = Yii::$app->session->get('contract');
        if ($session) {
            $session['delivery_id'] = $id;
            $session['payed_status'] = 2;
            Yii::$app->session->set('contract', $session);
        }
        $contract = new Contracts();
        if ($contract = $contract->saveFromSession()) {
            $this->layout = false;
            $cont = $this->renderPartial('pdf_template', [
                'id' => $contract->contract_id,
                'days' => $contract->days,
                'begin' => $contract->date_begin,
                'price' => number_format($contract->amount, "0", ' ', ' '),
                'end' => $contract->date_end,
                'fio' => $contract->user->fio,
                'passport' => $contract->user->possport_n,
                'phone' => $contract->user->phone
            ]);
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($cont);
            // Saves file on the server as 'filename.pdf'
            $mpdf->Output('uploads/pdf/' . $contract->id . '-' . $contract->contract_id . '.pdf', 'F');
            Yii::$app->session->setFlash('create-contract');
        }
        Yii::$app->session->setFlash('create-contract');
        return $this->redirect(Url::to(['/site/cabinet']));

//        $client->setAuth('0123', '123123123');
//        $payment = $client->createPayment(
//            array(
//                'amount' => array(
//                    'value' => $contract->amount,
//                    'currency' => 'RUB',
//                ),
//                'confirmation' => array(
//                    'type' => 'redirect',
//                    'return_url' => 'http://booking.com/site/cabinet?id=' . $contract->id,
//                ),
//                'capture' => true,
//                'description' => 'Договор №' . $contract->contract_id,
//            ),
//            uniqid('', true)
//        );


       // return $this->render('yandex');
    }

    public function actionContract()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->set('url', Url::to(['/site/contract']));
            return $this->redirect(Url::to(['/site/login']));
        }
        $session = Yii::$app->session->get('contract');
        if ($session) {
            $session['payed_status'] = 1;
            Yii::$app->session->set('contract', $session);
        }

        $contract = new Contracts();
        if ($contract = $contract->saveFromSession()) {
            $this->layout = false;
            $cont = $this->renderPartial('pdf_template', [
                'id' => $contract->contract_id,
                'days' => $contract->days,
                'begin' => $contract->date_begin,
                'price' => number_format($contract->amount, "0", ' ', ' '),
                'end' => $contract->date_end,
                'fio' => $contract->user->fio,
                'passport' => $contract->user->possport_n,
                'phone' => $contract->user->phone
            ]);
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($cont);
            // Saves file on the server as 'filename.pdf'
            $mpdf->Output('uploads/pdf/' . $contract->id . '-' . $contract->contract_id . '.pdf', 'F');

            Yii::$app->session->setFlash('create-contract');
            return $this->redirect(Url::to(['/site/cabinet']));
        }
        return false;

    }

    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            if (!empty(Yii::$app->session['url'])) {
                return $this->redirect(Yii::$app->session['url']);
            }
            return $this->goHome();
        }

        $this->layout = 'login';
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    public function actionSignup()
    {
        $model = new UserProfile();
        $user = new User();
        if ($user->load(Yii::$app->getRequest()->post())) {
            $user->username = $user->email;
            $user->status = User::STATUS_INACTIVE;
            $user->user_type = User::TYPE_CLIENT;
            if ($user->saveModel() && $model->load(Yii::$app->request->post())) {
                $model->user_id = $user->id;
                if ($model->save(false)) {
                    return $this->render('send');
                }
            }
        }
        $this->layout = 'login';

        return $this->render('signup', [
            'model' => $model,
            'user' => $user
        ]);
    }

    public function actionAuth($key)
    {
        /* @var $user User */
        $user = User::findOne(['auth_key' => $key]);
        if ($user->status == User::STATUS_INACTIVE) {
            $user->status = User::STATUS_ACTIVE;
            if ($user->save(false)) {
                $login = new Login();
                $login->username = $user->username;
                $login->password = $user->old_password;
                if ($login->login()) {
                    if (!empty(Yii::$app->session['url'])) {
                        return $this->redirect(Yii::$app->session['url']);
                    }
                    return $this->goHome();
                }
            } else {
                $errors = $user->firstErrors;
                throw new UserException(reset($errors));
            }
        }
        return $this->goHome();
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    public function actionCabinet($id = null)
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->set('url', Url::to(['/site/cabinet']));
            return $this->redirect(Url::to(['/site/login']));
        }
        // Profile
        $user = User::findOne(Yii::$app->user->id);
        $model = UserProfile::findOne(['user_id' => $user->id]);
        if ($user->load(Yii::$app->getRequest()->post()) && $user->save(false)) {
            if ($model->load(Yii::$app->getRequest()->post()) && $model->save(false)) {
                Yii::$app->session->setFlash('cabinet');
                return $this->refresh();
            }
        }

        // Contacts
        $contracts = Contracts::find()->where(['user_id' => $model->id])->all();
        $address = Address::find()->where(['user_id' => $user->id])->all();
        $newAddress = new Address();

        if (!empty($id)) {
            Yii::$app->session->setFlash('create-contract');
        }


        return $this->render('cabinet', [
            'user' => $user,
            'model' => $model,
            'contracts' => $contracts,
            'address' => $address,
            'newAddress' => $newAddress

        ]);
    }

    public function actionNewAddress()
    {
        $newAddress = new Address();
        if ($newAddress->load(Yii::$app->getRequest()->post())) {
            $newAddress->user_id = Yii::$app->user->id;
            $newAddress->save(false);
            Yii::$app->session->setFlash('newAddress');
            $address = Address::find()->where(['user_id' => Yii::$app->user->id])->all();
            return $this->renderAjax('ajax/addresses', [
                'address' => $address
            ]);
        }
        return false;
    }

    public function actionChangeAddress($id)
    {
        $address = Address::findOne($id);
        if ($address) {
            return $this->renderAjax('ajax/change-address', [
                'newAddress' => $address
            ]);
        }
        return false;
    }

    public function actionAddressChanged()
    {
        $id = Yii::$app->request->post('id');
        $address = Address::findOne($id);

        if ($address->load(Yii::$app->getRequest()->post()) && $address->save()) {
            $address = Address::find()->where(['user_id' => Yii::$app->user->id])->all();
            Yii::$app->session->setFlash('addressChanged');
            return $this->renderAjax('ajax/addresses', [
                'address' => $address
            ]);
        }
        return false;
    }

    public function actionAddressDelete($id)
    {
        $address = Address::findOne($id);

        if ($address->delete()) {
            $address = Address::find()->where(['user_id' => Yii::$app->user->id])->all();
            Yii::$app->session->setFlash('deleted');
            return $this->renderAjax('ajax/addresses', [
                'address' => $address
            ]);
        }
        return false;
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * AJAX
     * Show address on Map
     * @param $id
     * @return array|bool
     */
    public function actionShowMap($id)
    {
        $marker = Markers::findOne($id);
        if ($marker) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'lat' => $marker->lat,
                'lng' => $marker->lng,
            ];
        }
        return false;
    }

    /** AJAX
     * @return mixed
     */
    public function actionCountAdd()
    {
        $session = Yii::$app->session->get('contract');
        if ($session) {
            $amount = $session['amount'];
            $count = $session['count'];
            $session['amount'] = $amount + ($amount / $count);
            $session['count'] = $count + 1;
            Yii::$app->session->set('contract', $session);
            return number_format($session['amount'], '0', '', ' ') . ' руб.';
        }

        return false;
    }

    /** AJAX
     * @return mixed
     */
    public function actionCountMinus()
    {
        $session = Yii::$app->session->get('contract');
        if ($session) {
            $amount = $session['amount'];
            $count = $session['count'];
            $session['amount'] = $amount - ($amount / $count);
            $session['count'] = $count - 1;
            Yii::$app->session->set('contract', $session);
            return number_format($session['amount'], '0', '', ' ') . ' руб.';
        }
        return false;
    }


    public function actionProducts()
    {
        $product = ProductPage::findOne(1);

        return $this->render('products', [
            'product' => $product
        ]);
    }

    // Simple Pages
    public function actionProduct($id)
    {
        $product = ProductPage::findOne($id);
        $tariff = Tariff::find()->limit(3)->all();
        $markers = Markers::find()->all();
        $model = new Contracts();



        return $this->render('product', [
            'product' => $product,
            'tariff' => $tariff,
            'markers' => $markers,
            'model' => $model,
        ]);
    }

    public function actionFaq()
    {
        $category = FaqCategory::find()->where(['status' => 1])->all();

        return $this->render('faq',[
            'category' => $category
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $about = About::findOne(1);
        $advantages = Advantages::findOne(1);
        $socials = Socials::find()->where(['status' => 1])->all();
        return $this->render('contact', [
            'about' => $about,
            'advantages' => $advantages,
            'socials' => $socials
        ]);
    }


}
