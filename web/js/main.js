// Home page |  Add and minus buttons
$(document).on('click', '.btn-plus', function (e) {
    e.preventDefault();
    var val = parseInt($('#count').val())
    $('#count').val(val + 1);
});
$(document).on('click', '.btn-minus', function (e) {
    e.preventDefault();
    var val = parseInt($('#count').val())
    if (val > 1)
        $('#count').val(val - 1);
});

// Home Page | show map when selected address
function showMap() {
    var id = $('#contracts-marker_id').val()
    $.ajax({
        url: '/site/show-map',
        type: 'get',
        data: {id: id},
        success: function (res) {
            console.log(res.lat);
            console.log(res);
            $('#lat').attr('value', res.lat);
            $('#lng').attr('value', res.lng);
            initMap();
            $('.main-map').removeClass('hidden');
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
}

// Add and minus buttons |  Order page
$(document).on('click', '.btn-plus-order', function (e) {
    e.preventDefault();
    var val = parseInt($('#count').val())
    $.ajax({
        url: '/site/count-add',
        success: function (res) {
            $('#count').val(val + 1);
            $('.order-amount').html(res)
        },
        error: function () {
            alert('Ошибка на сервере')
        }
    })

});
// Minus count of product |  Order page
$(document).on('click', '.btn-minus-order', function (e) {
    e.preventDefault();
    var val = parseInt($('#count').val())
    if (val > 1) {
        $.ajax({
            url: '/site/count-minus',
            success: function (res) {
                $('#count').val(val - 1);
                $('.order-amount').html(res)
            },
            error: function () {
                alert('Ошибка на сервере')
            }
        })
    }
});

// Change address
$(document).on('click', '.change-address', function (e) {
    //e.preventDefault();
    var id = $(this).data('id');

    if (id > 0) {
        $.ajax({
            url: '/site/change-address',
            data: {id: id},
            success: function (res) {
                $('#changeAddress').html(res)
            },
            error: function () {
                alert('Ошибка на сервере')
            }
        })
    }
});

// Add new delivery Address
$(document).on('submit', '#addAddress', function (e) {
    e.preventDefault()
    var form = $(this);
    $.ajax({
        url: '/site/new-address',
        data: form.serialize(),
        type: 'post',
        success: function (res) {
            $('#addresses').html(res);
            $('#loginModal').modal('toggle');
        },
        error: function () {
            alert('Ошибка на сервере')
        }
    })
});

// Change Delivery Address Ajax
$(document).on('submit', '#changeForm', function (e) {
    e.preventDefault()
    var form = $(this);
    $.ajax({
        url: '/site/address-changed',
        data: form.serialize(),
        type: 'post',
        success: function (res) {
            $('#addresses').html(res);
            $('#changeModel').modal('toggle');
        },
        error: function () {
            alert('Ошибка на сервере')
        }
    })
});

// Delete Delivery address Ajax
$(document).on('click', '.delete-address', function (e) {
    e.preventDefault()
    var id = $(this).data('id')
    $.ajax({
        url: '/site/address-delete',
        data: {id: id},
        type: 'get',
        success: function (res) {
            $('#addresses').html(res);
        },
        error: function () {
            alert('Ошибка на сервере')
        }
    })

});

// Choose Delivery Address
$(document).on('click', '.address-cursor', function () {
    $('.address-cursor').removeClass('checked');
    $(this).addClass('checked');
    $(this).find('.check-address').prop('checked', true);
    $('.btn-yandex').removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
});

// If from office - Checkbox | Page Address
//$(document).on('change','.checkOffice', function () {
    // if($(this).is(':checked')){
    //     $('.check-address').prop('checked', false);
    //     $('.addr').removeClass('address-cursor').removeClass('checked').addClass('add-disable');
    //     $('.add-address').attr('disabled', 'disabled');
    //     $('.btn-yandex').removeClass('btn-default')
    //         .addClass('btn-primary')
    //         .removeAttr('disabled')
    //         .removeClass('btn-or-guest')
    //         .attr('href','/site/contract');
    // } else {
    //     $('.addr').removeClass('add-disable').addClass('address-cursor');
    //     $('.add-address').removeAttr('disabled');
    //     $('.btn-yandex').removeClass('btn-primary')
    //         .addClass('btn-default')
    //         .addClass('btn-or-guest')
    //         .attr('disabled','disabled')
    //         .attr('href','/site/yandex');
    // }
//});

// Button after choose delivery address
$(document).on('click', '.btn-or-guest', function (e) {
    e.preventDefault();
    var id = $('input[name=radioAddress]:checked', '.radio').val();
    window.location.href = '/site/yandex?id=' + id;
});

// Page Guest
// Checked from office
$(document).on('change','.officeAddress', function () {
    if($(this).is(':checked')) {
        $('#address-city').attr('disabled','disabled');
        $('#address-address').attr('disabled','disabled');
        $('#address-dom').attr('disabled','disabled');
        $('.panel-collapse').collapse('hide');
        $('.panel-body').addClass('hidden')
    } else {
        $('#address-city').removeAttr('disabled');
        $('#address-address').removeAttr('disabled');
        $('#address-dom').removeAttr('disabled');
        $('.panel-body').removeClass('hidden');
        $('.panel-collapse').collapse('show');

    }
});