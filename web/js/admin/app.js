// Dropify for Image UPLOAD
$('.dropify').dropify({
    messages: {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove': 'Remove',
        'error': 'Ooops, something wrong happended.'
    }
});

//LOADing
function runLoader() {
    $('.loader').removeClass('hidden');
}

function stopLoader() {
    $('.loader').addClass('hidden');
}

var tag = $('#admin-content p:first:has("a")');
$('.page-titles .col-md-5.align-self-center').html(tag.html());
tag.remove();

//////////////////////////////////////////////
//                                          //
//            PAGE CONTRACTS                //
//                                          //
//////////////////////////////////////////////

// User Select
$(document).on('keyup', '.userSelect', function (e) {
    e.preventDefault();
    var value = this.value;
    $.ajax({
        url: '/admin/contracts/get-user',
        type: 'get',
        data: {value: value},
        beforeSend: function () {
            runLoader();
        },
        success: function (res) {
            $('#userSelect').html(res);
        },
        complete: function () {
            stopLoader();
        },
        error: function (res) {
            //alert("Ошибка сервера")
        }
    })
});
// Form submit button | Create New User
$(document).on('click', '.submit-crete', function (e) {
    e.preventDefault();
    var form = $('#formSubmit');
    form.submit();

});

// Form submit
$('#formSubmit').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),  // Url::to(['/admin/contracts/create-user'])
        type: form.attr('method'), // GET
        data: form.serialize(),    // $user, $userProfile
        beforeSend: function () {
            runLoader();
        },
        success: function (res) {
            if (res == 2) {
                alert('Ошибка при создании клиента. Пожалуйста, заполните все поля правильно!')
            } else {
                $('#userSelect').html(res);
                $('#mediumModal').modal('toggle');
            }
        },
        complete: function () {
            stopLoader();
        },
        error: function () {
            alert('Ошибка при создании Клиента');
        }

    });
});

// Select Marker
$(document).on('change', '.selectMarker', function (e) {
    e.preventDefault();
    var id = $(this).val();
    $.ajax({
        url: '/admin/contracts/get-worker',
        type: 'get',
        data: {id: id},
        beforeSend: function () {
            runLoader();
        },
        success: function (res) {
            $('#setWorker').html(res);
        },
        complete: function () {
            stopLoader();
        },
        error: function (res) {
            //alert("Ошибка сервера")
        }
    })

});

// User Select
$(document).on('keyup', '.productSelectInput', function (e) {
    e.preventDefault();
    var value = this.value;

    $.ajax({
        url: '/admin/contracts/get-product',
        type: 'get',
        data: {value: value},
        beforeSend: function () {
            runLoader();
        },
        success: function (res) {
            $('#productSelect').html(res);
        },
        complete: function () {
            stopLoader();
        },
        error: function (res) {
            //alert("Ошибка сервера")
        }
    })
});

//////////////////////////////////////////////
//                                          //
//            PAGE WORKER                   //
//                                          //
//////////////////////////////////////////////

$(document).on('change','.selected_marker', function () {
   var value = $('#marker_select').val();
   $.ajax({
       url: '/admin/worker/user-position',
       type: 'get',
       data: {id: value},
       beforeSend: function () {
           runLoader();
       },
       success: function (res) {
           $('#positions').html(res);
       },
       complete: function () {
           stopLoader();
       },
       error: function (res) {
           alert("Ошибка сервера")
       }
   })

});