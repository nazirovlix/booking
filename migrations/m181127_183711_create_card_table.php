<?php

use yii\db\Migration;

/**
 * Handles the creation of table `card`.
 */
class m181127_183711_create_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('card', [
            'id' => $this->primaryKey(),
            'card_number' => $this->string()->notNull(),
            'tariff' => $this->string(),
            'operator' => $this->string(),
            'extension' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('card');
    }
}
