<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advantages`.
 */
class m181123_194358_create_advantages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('advantages', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'content' => $this->text(),
            'icon1' => $this->string(),
            'title1' => $this->string(),
            'content1' => $this->text(),
            'icon2' => $this->string(),
            'title2' => $this->string(),
            'content2' => $this->text(),
            'icon3' => $this->string(),
            'title3' => $this->string(),
            'content3' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('advantages');
    }
}
