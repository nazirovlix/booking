<?php

use yii\db\Migration;

/**
 * Handles the creation of table `markers`.
 */
class m181123_175825_create_markers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('markers', [
            'id' => $this->primaryKey(),
            'city' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'lat' => $this->string(),
            'lng' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('markers');
    }
}
