<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contracts`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `user`
 * - `products`
 */
class m181123_193736_create_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contracts', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNUll(),
            'date_begin' => $this->string()->notNull(),
            'date_end' => $this->string()->notNull(),
            'amount' => $this->integer(),
            'card_number' => $this->string(),
            'contract_pdf' => $this->string(),
            'payed_status' => $this->smallInteger(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(7),
            'updated_at' => $this->integer(7),
        ]);

        // creates index for column `worder_id`
        $this->createIndex(
            'idx-contracts-worker_id',
            'contracts',
            'worker_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-contracts-worker_id',
            'contracts',
            'worker_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-contracts-user_id',
            'contracts',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-contracts-user_id',
            'contracts',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `product_id`
        $this->createIndex(
            'idx-contracts-product_id',
            'contracts',
            'product_id'
        );

        // add foreign key for table `products`
        $this->addForeignKey(
            'fk-contracts-product_id',
            'contracts',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-contracts-worker_id',
            'contracts'
        );

        // drops index for column `worker_id`
        $this->dropIndex(
            'idx-contracts-worker_id',
            'contracts'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-contracts-user_id',
            'contracts'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-contracts-user_id',
            'contracts'
        );

        // drops foreign key for table `products`
        $this->dropForeignKey(
            'fk-contracts-product_id',
            'contracts'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-contracts-product_id',
            'contracts'
        );

        $this->dropTable('contracts');
    }
}
