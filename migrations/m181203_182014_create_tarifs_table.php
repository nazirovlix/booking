<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tarifs`.
 */
class m181203_182014_create_tarifs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tarifs', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'day_count' => $this->integer(),
            'price' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tarifs');
    }
}
