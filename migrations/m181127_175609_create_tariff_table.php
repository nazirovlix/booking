<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tariff`.
 */
class m181127_175609_create_tariff_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tariff', [
            'id'        => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->integer()->notNull(),
            'day' => $this->integer(),
            'title' => $this->string(),
            'limit' => $this->string(),
            'descriptions' => $this->string(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tariff');
    }
}
