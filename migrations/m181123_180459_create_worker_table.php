<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `positions`
 * - `markers`
 */
class m181123_180459_create_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'fio' => $this->string()->notNull(),
            'passport_n' => $this->string()->notNull(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'bday' => $this->string(),
            'snils' => $this->string(),
            'position_id' => $this->integer(),
            'marker_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-worker-user_id',
            'worker',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-worker-user_id',
            'worker',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `position_id`
        $this->createIndex(
            'idx-worker-position_id',
            'worker',
            'position_id'
        );

        // add foreign key for table `positions`
        $this->addForeignKey(
            'fk-worker-position_id',
            'worker',
            'position_id',
            'positions',
            'id',
            'RESTRICT'
        );

        // creates index for column `marker_id`
        $this->createIndex(
            'idx-worker-marker_id',
            'worker',
            'marker_id'
        );

        // add foreign key for table `markers`
        $this->addForeignKey(
            'fk-worker-marker_id',
            'worker',
            'marker_id',
            'markers',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-worker-user_id',
            'worker'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-worker-user_id',
            'worker'
        );

        // drops foreign key for table `positions`
        $this->dropForeignKey(
            'fk-worker-position_id',
            'worker'
        );

        // drops index for column `position_id`
        $this->dropIndex(
            'idx-worker-position_id',
            'worker'
        );

        // drops foreign key for table `markers`
        $this->dropForeignKey(
            'fk-worker-marker_id',
            'worker'
        );

        // drops index for column `marker_id`
        $this->dropIndex(
            'idx-worker-marker_id',
            'worker'
        );

        $this->dropTable('worker');
    }
}
