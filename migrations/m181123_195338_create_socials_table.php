<?php

use yii\db\Migration;

/**
 * Handles the creation of table `socials`.
 */
class m181123_195338_create_socials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('socials', [
            'id' => $this->primaryKey(),
            'icon' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'order' => $this->smallInteger(),
            'status' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('socials');
    }
}
