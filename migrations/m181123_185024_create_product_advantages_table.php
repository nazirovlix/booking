<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_advantages`.
 * Has foreign keys to the tables:
 *
 * - `products`
 */
class m181123_185024_create_product_advantages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_advantages', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'icon' => $this->string(),
            'title' => $this->string(),
            'content' => $this->text(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx-product_advantages-product_id',
            'product_advantages',
            'product_id'
        );

        // add foreign key for table `products`
        $this->addForeignKey(
            'fk-product_advantages-product_id',
            'product_advantages',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `products`
        $this->dropForeignKey(
            'fk-product_advantages-product_id',
            'product_advantages'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-product_advantages-product_id',
            'product_advantages'
        );

        $this->dropTable('product_advantages');
    }
}
