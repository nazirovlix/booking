<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m181222_080743_create_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'city' => $this->string()->notnull(),
            'address' => $this->string()->notNull(),
            'dom' => $this->string(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-address-user_id',
            'address',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-address-user_id',
            'address',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-address-user_id',
            'address'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-address-user_id',
            'address'
        );

        $this->dropTable('address');
    }
}
