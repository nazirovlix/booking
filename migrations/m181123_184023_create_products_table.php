<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 * Has foreign keys to the tables:
 *
 * - `marker`
 */
class m181123_184023_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'serial_number' => $this->string()->notNull(),
            'extension_number' => $this->string()->notNull(),
            'barcode' => $this->string(),
            'image' => $this->string(),
            'description' => $this->text(),
            'content' => $this->text(),
            'amount' => $this->integer()->notNull(),
            'free' => $this->smallInteger(6)->defaultValue(1),
            'status' => $this->smallInteger(6),
            'marker_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        // creates index for column `marker_id`
        $this->createIndex(
            'idx-products-marker_id',
            'products',
            'marker_id'
        );

        // add foreign key for table `markers`
        $this->addForeignKey(
            'fk-products-marker_id',
            'products',
            'marker_id',
            'markers',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `markers`
        $this->dropForeignKey(
            'fk-products-marker_id',
            'products'
        );

        // drops index for column `marker_id`
        $this->dropIndex(
            'idx-products-marker_id',
            'products'
        );

        $this->dropTable('products');
    }
}
