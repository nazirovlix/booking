<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tips`.
 */
class m181123_194813_create_tips_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tips', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'content' => $this->text(),
            'image' => $this->string(),
            'status' => $this->smallInteger(6),
            'meta_key' => $this->string(),
            'meta_descriptions' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tips');
    }
}
